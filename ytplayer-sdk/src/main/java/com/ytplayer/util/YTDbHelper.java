package com.ytplayer.util;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import com.helper.util.BaseDatabaseHelper;
import com.ytplayer.YTPlayer;
import com.ytplayer.adapter.YTVideoModel;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.Callable;


public class YTDbHelper extends BaseDatabaseHelper {

    public static final String DB_NAME = "yt.sqlite";
    public static final int DATABASE_VERSION = 2;

    static SQLiteDatabase db;
    Context context;

    private static final String TABLE_WATCH_LIST = "watch_list";
    private static final String TABLE_PLAY_LIST = "play_list";
    private static final String COLUMN_ID = "id";
    private static final String COLUMN_VIDEO_ID = "video_id";
    private static final String COLUMN_TITLE = "title";
    private static final String COLUMN_VIDEO_TIME = "video_time";
    private static final String COLUMN_VIDEO_DURATION = "video_duration";
    private static final String COLUMN_CHANNEL_ID = "channel_id";
    private static final String COLUMN_IS_READ = "is_read";
    private static final String COLUMN_IS_FAV = "is_fav";
    private static final String COLUMN_IS_ACTIVE = "is_active";
    private static final String COLUMN_JSON_DATA = "json_data";

    private static final String COLUMN_CHANNEL_NAME = "channel_name";
    private static final String COLUMN_IMAGE = "image";
    private static final String COLUMN_ITEM_COUNT = "item_count";
    private static final String COLUMN_CREATED_AT = "created_at";
    private static final String COLUMN_CAT_ID = "cat_id";
    private static final String COLUMN_ITEM_TYPE = "item_type";
    public static final int ACTIVE = 1;
    public static final int IN_ACTIVE = 0;

    private static final String CREATE_TABLE_WATCH_LIST = "CREATE TABLE IF NOT EXISTS watch_list (" +
            "    id          INTEGER PRIMARY KEY AUTOINCREMENT," +
            "    video_id    VARCHAR," +
            "    title       VARCHAR," +
            "    channel_id  VARCHAR," +
            "    video_time  INTEGER DEFAULT (0)," +
            "    video_duration  INTEGER DEFAULT (0)," +
            "    is_read     INTEGER DEFAULT (0)," +
            "    is_fav      INTEGER DEFAULT (0)," +
            "    is_active   INTEGER DEFAULT (0)," +
            "    json_data   VARCHAR" +
            ");";

    private static final String CREATE_TABLE_PLAY_LIST = "CREATE TABLE IF NOT EXISTS play_list (" +
            "    id          INTEGER PRIMARY KEY AUTOINCREMENT," +
            "    cat_id     INTEGER DEFAULT (0)," +
            "    video_id    VARCHAR," +
            "    title       VARCHAR," +
            "    channel_id  VARCHAR," +
            "    channel_name  VARCHAR," +
            "    image  VARCHAR," +
            "    item_count  VARCHAR," +
            "    item_type  VARCHAR," +
            "    is_read     INTEGER DEFAULT (0)," +
            "    is_fav      INTEGER DEFAULT (0)," +
            "    json_data   VARCHAR," +
            "    created_at     VARCHAR" +
            ");";


    private YTDbHelper(Context context) {
        super(context, DB_NAME, null, DATABASE_VERSION);
        this.context = context;
    }

    private static YTDbHelper instance;

    public static YTDbHelper getInstance(Context context) {
        if (instance == null)
            instance = new YTDbHelper(context);
        return instance;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE_WATCH_LIST);
        db.execSQL(CREATE_TABLE_PLAY_LIST);
    }


    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        switch (oldVersion) {
            case 1:
                db.execSQL(CREATE_TABLE_PLAY_LIST);
        }
    }

    public void callDBFunction(Callable<Void> function) {
        try {
            db = getDB();
            if (db != null) {
                if (!db.inTransaction()) {
                    db.beginTransaction();
                }
                function.call();
                if (db.inTransaction()) {
                    db.setTransactionSuccessful();
                    if (db.inTransaction()) {
                        db.endTransaction();
                    }
                }

            }
        } catch (Exception e) {
            try {
                db = getDB();
                if (db != null) {
                    if (db.inTransaction()) {
                        db.setTransactionSuccessful();
                        if (db.inTransaction()) {
                            db.endTransaction();
                        }
                    }
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        } finally {
            try {
                if (db != null && db.inTransaction())
                    db.endTransaction();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    private SQLiteDatabase getDB() throws Exception {
        SQLiteDatabase database = getWritableDatabase();
        if (database == null) {
            instance = new YTDbHelper(YTPlayer.getInstance().getContext());
        }
        return database;
    }


    public void insertVideoInWatchList(YTVideoModel model) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(COLUMN_VIDEO_ID, model.getVideoId());
        contentValues.put(COLUMN_TITLE, model.getTitle());
        contentValues.put(COLUMN_VIDEO_TIME, model.getVideoTime());
        contentValues.put(COLUMN_VIDEO_DURATION, model.getVideoDuration());
        contentValues.put(COLUMN_CHANNEL_ID, model.getChannelId());
        contentValues.put(COLUMN_IS_READ, ACTIVE);
        contentValues.put(COLUMN_JSON_DATA, model.toJson());

        String where = COLUMN_VIDEO_ID + "='" + model.getVideoId() + "'";
        Cursor cursor = db.query(TABLE_WATCH_LIST, null, where, null, null, null, null);

        try {
            if (!(cursor != null && cursor.getCount() > 0 && cursor.moveToFirst())) {
                if (cursor != null) {
                    cursor.close();
                }
                try {
                    db.insertOrThrow(TABLE_WATCH_LIST, null, contentValues);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                try {
                    db.update(TABLE_WATCH_LIST, contentValues, where, null);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void deleteVideoFromWatchList(int videoId) {
        try {
            String where = COLUMN_VIDEO_ID + "='" + videoId + "'";
            db.delete(TABLE_WATCH_LIST, where, null);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void updateWatchListRead(String videoId, boolean isRead) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(COLUMN_IS_READ, isRead ? ACTIVE : IN_ACTIVE);
        String where = COLUMN_VIDEO_ID + "='" + videoId + "'";
        try {
            int i = db.update(TABLE_WATCH_LIST, contentValues, where, null);
            Logger.log("updateNotification Read i -- " + i);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void updateWatchListFavourite(String videoId, boolean isFavourite) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(COLUMN_IS_FAV, isFavourite ? ACTIVE : IN_ACTIVE);
        String where = COLUMN_VIDEO_ID + "='" + videoId + "'";
        try {
            int i = db.update(TABLE_WATCH_LIST, contentValues, where, null);
            Logger.log("updateNotification Read i -- " + i);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public HashMap<String, YTVideoModel> fetchWatchList(String channelId) {
        HashMap<String, YTVideoModel> watchList = new HashMap<>();
        String where = COLUMN_CHANNEL_ID + "='" + channelId + "'";
        Cursor cursor = db.query(TABLE_WATCH_LIST, null, where, null, null, null, COLUMN_ID + " DESC");
        if (cursor != null && cursor.getCount() > 0 && cursor.moveToFirst()) {
            do {
                YTVideoModel bean = new YTVideoModel();
                bean.setVideoId(cursor.getString(cursor.getColumnIndex(COLUMN_VIDEO_ID)));
                bean.setTitle(cursor.getString(cursor.getColumnIndex(COLUMN_TITLE)));
                bean.setVideoTime(cursor.getInt(cursor.getColumnIndex(COLUMN_VIDEO_TIME)));
                bean.setVideoDuration(cursor.getInt(cursor.getColumnIndex(COLUMN_VIDEO_DURATION)));
                bean.setChannelId(cursor.getString(cursor.getColumnIndex(COLUMN_CHANNEL_ID)));
                bean.setIsRead(cursor.getInt(cursor.getColumnIndex(COLUMN_IS_READ)));
                bean.setIsFav(cursor.getInt(cursor.getColumnIndex(COLUMN_IS_FAV)));
                bean.setJsonData(cursor.getString(cursor.getColumnIndex(COLUMN_JSON_DATA)));
                watchList.put(bean.getVideoId(), bean);
            } while (cursor.moveToNext());
        }
        if (cursor != null) {
            cursor.close();
        }
        return watchList;
    }

    public void insertPlayList(int catId, YTVideoModel model) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(COLUMN_CAT_ID, catId);
        contentValues.put(COLUMN_VIDEO_ID, model.getVideoId());
        contentValues.put(COLUMN_TITLE, model.getTitle());
        contentValues.put(COLUMN_CHANNEL_ID, model.getChannelId());
        contentValues.put(COLUMN_CHANNEL_NAME, model.getChannelTitle());
        contentValues.put(COLUMN_IMAGE, model.getImage());
        contentValues.put(COLUMN_ITEM_COUNT, model.getTotalResults());
        contentValues.put(COLUMN_IS_READ, ACTIVE);
        contentValues.put(COLUMN_IS_FAV, model.getIsFav());
        contentValues.put(COLUMN_ITEM_TYPE, model.getYtType().toString());
        contentValues.put(COLUMN_CREATED_AT, getDbDateTime());
        contentValues.put(COLUMN_JSON_DATA, model.toJson());

        String where = COLUMN_VIDEO_ID + "='" + model.getVideoId() + "'";
        Cursor cursor = db.query(TABLE_PLAY_LIST, null, where, null, null, null, null);

        try {
            if (!(cursor != null && cursor.getCount() > 0 && cursor.moveToFirst())) {
                if (cursor != null) {
                    cursor.close();
                }
                try {
                    db.insertOrThrow(TABLE_PLAY_LIST, null, contentValues);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                try {
                    db.update(TABLE_PLAY_LIST, contentValues, where, null);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public List<YTVideoModel> getAllBookmarkVideos() {
        return getAllBookmarks(0, YTType.VIDEO);
    }

    public List<YTVideoModel> getAllBookmarkPlayList(int catId) {
        return getAllBookmarks(catId, YTType.PLAYLIST);
    }

    public List<YTVideoModel> getAllBookmarks(int catId, YTType itemType) {
        List<YTVideoModel> playList = new ArrayList<>();
        Cursor cursor;
        if(itemType == YTType.VIDEO) {
            String query = "SELECT a.*, b.video_time, b.video_duration FROM play_list  a LEFT JOIN watch_list b ON a.video_id=b.video_id WHERE a.is_fav='1' AND a.item_type='VIDEO' order By created_at DESC";
            cursor  = db.rawQuery(query, null);
        }else {
            String where = COLUMN_IS_FAV + "='" + ACTIVE + "' AND " + COLUMN_ITEM_TYPE + "='" + itemType.toString() + "'";
            if(catId > 0){
                where += " AND " + COLUMN_CAT_ID + "='" + catId + "'";
            }
            cursor = db.query(TABLE_PLAY_LIST, null, where, null, null, null, COLUMN_CREATED_AT + " DESC");
        }
        if (cursor != null && cursor.getCount() > 0 && cursor.moveToFirst()) {
            do {
                YTVideoModel bean = new YTVideoModel();
                bean.setAutoId(cursor.getInt(cursor.getColumnIndex(COLUMN_ID)));
                bean.setVideoId(cursor.getString(cursor.getColumnIndex(COLUMN_VIDEO_ID)));
                bean.setTitle(cursor.getString(cursor.getColumnIndex(COLUMN_TITLE)));
                bean.setChannelId(cursor.getString(cursor.getColumnIndex(COLUMN_CHANNEL_ID)));
                bean.setChannelTitle(cursor.getString(cursor.getColumnIndex(COLUMN_CHANNEL_NAME)));
                bean.setImage(cursor.getString(cursor.getColumnIndex(COLUMN_IMAGE)));
                bean.setTotalResults(cursor.getString(cursor.getColumnIndex(COLUMN_ITEM_COUNT)));
                bean.setIsRead(cursor.getInt(cursor.getColumnIndex(COLUMN_IS_READ)));
                bean.setIsFav(cursor.getInt(cursor.getColumnIndex(COLUMN_IS_FAV)));
                bean.setJsonData(cursor.getString(cursor.getColumnIndex(COLUMN_JSON_DATA)));
                if(cursor.getColumnIndex(COLUMN_VIDEO_TIME) != -1) {
                    bean.setVideoTime(cursor.getInt(cursor.getColumnIndex(COLUMN_VIDEO_TIME)));
                }
                if(cursor.getColumnIndex(COLUMN_VIDEO_DURATION) != -1) {
                    bean.setVideoDuration(cursor.getInt(cursor.getColumnIndex(COLUMN_VIDEO_DURATION)));
                }
                playList.add(bean);
            } while (cursor.moveToNext());
        }
        if (cursor != null) {
            cursor.close();
        }
        return playList;
    }

    public HashMap<String, Integer> getAllBookmarkPlayListHashMap(int catId) {
        HashMap<String, Integer> watchList = new HashMap<>();
        List<YTVideoModel> playLists = getAllBookmarks(catId, YTType.PLAYLIST);
        for (YTVideoModel item : playLists){
            watchList.put(item.getVideoId(), item.getIsFav());
        }
        return watchList;
    }



    public void updatePlayListFavourite(String videoOrPlaylistId, boolean isFavourite) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(COLUMN_IS_FAV, isFavourite ? ACTIVE : IN_ACTIVE);
        String where = COLUMN_VIDEO_ID + "='" + videoOrPlaylistId + "'";
        try {
            int i = db.update(TABLE_PLAY_LIST, contentValues, where, null);
            Logger.log("updateNotification Read i -- " + i);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public int getPlayListFavouriteById(String videoOrPlaylistId) {
        String where = COLUMN_VIDEO_ID + "='" + videoOrPlaylistId + "'";
        Cursor cursor = db.query(TABLE_PLAY_LIST, null, where, null, null, null, null);
        int isFav = IN_ACTIVE;
        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();
            isFav = cursor.getInt(cursor.getColumnIndex(COLUMN_IS_FAV));
        }
        if (cursor != null) {
            cursor.close();
        }
        return isFav;
    }

    public String getDbDateTime() {
        return (new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault())).format(new Date());
    }

}
