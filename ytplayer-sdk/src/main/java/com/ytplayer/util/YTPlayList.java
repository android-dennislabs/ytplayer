package com.ytplayer.util;

import com.ytplayer.adapter.YTVideoModel;

import java.util.ArrayList;

/**
 * @author Created by Abhijit on 2/6/2018.
 */
public enum YTPlayList {
    instance;

    private ArrayList<YTVideoModel> playList;

    public static ArrayList<YTVideoModel> getPlayList() {
        return instance.playList;
    }

    public static void setPlayList(ArrayList<YTVideoModel> playList) {
        instance.playList = playList;
    }
}
