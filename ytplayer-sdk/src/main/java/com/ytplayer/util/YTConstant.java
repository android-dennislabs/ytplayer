package com.ytplayer.util;

import com.ytplayer.BuildConfig;

/**
 * @author Created by Abhijit on 2/6/2018.
 */
public interface YTConstant {
    String PLAYLIST = "playlist";
    String API_HOST = "api_host";
    String CAT_ID = "catId";
    String IS_SHOW_BOOKMARK = "isShowBookmark";
    String PLAYER_NAME = "playerName";
    String PLAYLIST_ID = "playlistId";
    String CHANNEL_ID = "channelId";
    String VIDEO_DETAIL = "videoDetail";
    String VIDEO = "video";
    String DATA = "data";
    String EXTRA_PROPERTY = "extra_property";
    boolean isLogEnabled = BuildConfig.DEBUG;

    String HOST_BASE = "host_video_other";
    int MIN_RESULTS_COUNT = 10;
    int MAX_RESULTS_COUNT = 40;

    int TOTAL_RESULT_SIZE = 35;
    String INVALID_PROPERTY = "Invalid Property";

    boolean IS_LOAD_VIDEO_STATICS = true;
    String NO_DATA = "No data";
    String NO_INTERNET_CONNECTION = "No Internet Connection";
    String POSITION = "position";
}
