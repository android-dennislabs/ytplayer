package com.ytplayer.util;

import com.ytplayer.adapter.YTVideoModel;

public interface YTListener {

    interface OnHistoryListener{
        void onMaintainHistory(YTVideoModel item);
    }
}
