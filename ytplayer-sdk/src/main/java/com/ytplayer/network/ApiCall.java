package com.ytplayer.network;


import android.text.TextUtils;

import com.config.config.ConfigConstant;
import com.config.config.ConfigManager;
import com.config.util.ConfigUtil;
import com.helper.callback.NetworkListener;
import com.ytplayer.YTPlayer;
import com.ytplayer.YTUtility;
import com.ytplayer.util.YTConstant;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * @author Created by Abhijit on 13-Dec-16.
 */
public class ApiCall {

    public interface ApiResponse{
        void onSuccess(String response);
        void onFailure(Exception e);
        default void onRetry(NetworkListener.Retry retryCallback){}
    }

    public static void request(String host, String method, Map<String, String> params, final ApiResponse callback){
        if(TextUtils.isEmpty(host)){
            host = YTConstant.HOST_BASE;
        }
        try {
            YTPlayer.getInstance().getConfigManager().getData(ConfigConstant.CALL_TYPE_GET, host
                    , method, params, new ConfigManager.OnNetworkCall() {
                        @Override
                        public void onComplete(boolean status, String data) {
                            if (status && !ConfigUtil.isEmptyOrNull(data)) {
                                callback.onSuccess(data);
                            } else {
                                callback.onFailure(new Exception("No Data"));
                            }
                        }

                        @Override
                        public void onRetry(NetworkListener.Retry retryCallback, Throwable error) {
                            callback.onRetry(retryCallback);
                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
            callback.onFailure(e);
        }
    }

//    public static final String IO_EXCEPTION = "IOException";
//
//    //GET network request
//    public static String GET(String url, String params){
//        return GET(url+params);
//    }
//    public static String GET(String url, List<KeyValuePair> params){
//        return GET(url,params,false);
//    }
//    public static String GET(String url, List<KeyValuePair> params, boolean hasEncoding){
//        String paramString = format2(params,hasEncoding ? "utf-8":null);
//        String finalUrl = url + "?" + paramString;
//        return GET(finalUrl);
//    }
//
//    public static String GET(String url){
//        YTUtility.log(url);
//        OkHttpClient client = new OkHttpClient.Builder()
//                .connectTimeout(15, TimeUnit.SECONDS)
//                .writeTimeout(15, TimeUnit.SECONDS)
//                .readTimeout(15, TimeUnit.SECONDS)
//                .build();
//        Request request = new Request.Builder()
//                .url(url)
//                .build();
//        try {
//            Response response = client.newCall(request).execute();
//            if(response.body()!=null){
////                YTUtility.log(response.body().string());
//                return response.body().string();
//            }else {
//                return IO_EXCEPTION;
//            }
//        }catch (IOException e){
//            return IO_EXCEPTION+" "+e.toString();
//        }
//    }
//
//
//
//
//    public static String format2(List<KeyValuePair> list, String encoding) {
//        StringBuilder result = new StringBuilder();
//
//        for (KeyValuePair parameter:list){
//            String encodedValue;
//            String encodedKey;
//            if(encoding!=null){
//                encodedKey = encode(parameter.getKey(), encoding);
//                encodedValue = parameter.getValue() != null?encode(parameter.getValue(), encoding):"";
//            }else{
//                encodedKey = parameter.getKey();
//                encodedValue = parameter.getValue();
//            }
//            if(result.length() > 0) {
//                result.append("&");
//            }
//
//            result.append(encodedKey);
//            result.append("=");
//            result.append(encodedValue);
//        }
//        return result.toString();
//    }
//
//    private static String decode(String content, String encoding) {
//        try {
//            return URLDecoder.decode(content, encoding != null?encoding:"ISO-8859-1");
//        } catch (UnsupportedEncodingException var3) {
//            throw new IllegalArgumentException(var3);
//        }
//    }
//
//    private static String encode(String content, String encoding) {
//        try {
//            return URLEncoder.encode(content, encoding != null?encoding:"ISO-8859-1");
//        } catch (UnsupportedEncodingException var3) {
//            throw new IllegalArgumentException(var3);
//        }
//    }
}