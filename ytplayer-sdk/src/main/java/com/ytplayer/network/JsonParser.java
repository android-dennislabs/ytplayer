package com.ytplayer.network;

import android.os.Build;
import android.text.TextUtils;

import androidx.annotation.RequiresApi;

import com.ytplayer.YTUtility;
import com.ytplayer.adapter.YTVideoModel;
import com.ytplayer.adapter.YTVideoStatistics;
import com.ytplayer.model.YTPlaylistVideoModel;
import com.ytplayer.util.YTConstant;
import com.ytplayer.util.YTType;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Created by Abhijit on 2/6/2018.
 */

public class JsonParser {

    public static YTVideoModel parsePlayListAndVideo(String response , List<YTPlaylistVideoModel> playlistVideoModels) {
        YTVideoModel model = new YTVideoModel();
        try {
            if ( playlistVideoModels != null && playlistVideoModels.size() > 0 ) {
                JSONObject jsonObject = new JSONObject(response);
                JSONArray playlistMetadata = jsonObject.optJSONArray("playlist_metadata");
                JSONArray videoMetadata = jsonObject.optJSONArray("video_metadata");

                ArrayList<YTVideoModel> playList = parsePlayListJSONArray( playlistMetadata);
                ArrayList<YTVideoModel> videoModels = parsePlayListItemsJsonArray(  videoMetadata);

                model.setList(new ArrayList<YTVideoModel>());
                for ( YTPlaylistVideoModel videoModel : playlistVideoModels ){
                    if ( videoModel != null ){
                        YTVideoModel model1 ;
                        switch ( videoModel.getYtType() ){
                            case PLAYLIST:
                                model1 = getYtVideoModel( playList , videoModel.getId() );
                                if ( model1 != null ){
                                    model.getList().add(model1);
                                }
                                break;
                            case VIDEO:
                                 model1 = getYtVideoModel( videoModels , videoModel.getId() );
                                if ( model1 != null ){
                                    model.getList().add(model1);
                                }
                                break;
                        }
                    }
                }
            }else {
                model.setError(YTConstant.NO_DATA);
            }
        } catch (JSONException e) {
            e.printStackTrace();
            model.setError(YTConstant.NO_DATA);
        }
        return model ;
    }

    public static YTVideoModel getYtVideoModel(ArrayList<YTVideoModel> list , String id){
        YTVideoModel model = null ;
        if ( !TextUtils.isEmpty( id )) {
            for ( YTVideoModel model1 : list ){
                if ( model1 != null && model1.getVideoId().equalsIgnoreCase(id) ){
                    return model1;
                }
            }
        }
        return model;
    }

    public static YTVideoModel parsePlayList(String response) {
        YTVideoModel jsonModel = new YTVideoModel();
        try {
//            jsonModel.setList(new ArrayList<YTVideoModel>());
            jsonModel.setList( parsePlayListJSONArray(  new JSONArray(response)));
        } catch (JSONException e) {
            e.printStackTrace();
            jsonModel.setError(YTConstant.NO_DATA);
        }
        return jsonModel;
    }

    public static ArrayList<YTVideoModel> parsePlayListJSONArray(JSONArray itemsArr) {
        ArrayList<YTVideoModel> list = new ArrayList<>();
        try {
            if ( itemsArr != null && itemsArr.length() > 0 ) {
                for (int i = 0; i < itemsArr.length(); i++) {
                    JSONObject item = itemsArr.getJSONObject(i);
                    if (!item.getString("channel_name").equals("")) {
                        YTVideoModel model = new YTVideoModel();
                        model.setYtType(YTType.PLAYLIST);
                        model.setVideoId(item.optString("playlist_id"));

                        model.setTotalResults(item.optString("playlist_item_count"));

                        if ( item.optJSONObject("thumbnails") != null ) {
                            JSONObject thumbnails = item.getJSONObject("thumbnails");
                            JSONObject medium = thumbnails.getJSONObject("medium");
                            model.setImage(medium.optString("url"));
                        }

                        model.setTitle(item.optString("playlist_title"));
                        model.setChannelId(item.optString("channel_id"));
                        model.setChannelTitle(item.optString("channel_name"));
                        list.add(model);
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return list ;
    }

    public static YTVideoModel parseSearchList(String response) {
        YTVideoModel jsonModel = new YTVideoModel();
        try {
            jsonModel.setList(new ArrayList<YTVideoModel>());
            JSONObject mainObject = new JSONObject(response);
            if (!mainObject.getString("nextPageToken").equals("")) {
                jsonModel.setNextPageToken(mainObject.getString("nextPageToken"));
                JSONObject pageInfo = mainObject.getJSONObject("pageInfo");
                jsonModel.setTotalResults(pageInfo.optString("totalResults"));
                JSONArray itemsArr = mainObject.getJSONArray("items");
                for (int i = 0; i < itemsArr.length(); i++) {
                    YTVideoModel model = new YTVideoModel();
                    JSONObject item = itemsArr.getJSONObject(i);
                    JSONObject id = item.getJSONObject("id");
                    model.setVideoId(id.optString("videoId"));
                    JSONObject snippet = item.getJSONObject("snippet");
                    JSONObject thumbnails = snippet.getJSONObject("thumbnails");
                    JSONObject medium = thumbnails.getJSONObject("medium");
                    model.setPublishedAt(snippet.optString("publishedAt"));
                    model.setTitle(snippet.optString("title"));
                    model.setChannelId(snippet.optString("channelId"));
                    model.setChannelTitle(snippet.optString("channelTitle"));
                    model.setDescription(snippet.optString("description"));
                    model.setImage(medium.optString("url"));
                    jsonModel.getList().add(model);
                }
            } else if (!mainObject.getString("error").equals("")) {
                JSONObject errorObject = mainObject.getJSONObject("error");
                jsonModel.setError(errorObject.getString("message"));
            }
        } catch (JSONException e) {
            e.printStackTrace();
            jsonModel.setError(YTConstant.NO_DATA);
        }
        return jsonModel;
    }

    public static ArrayList<YTVideoModel> parsePlayListItemsJsonArray(JSONArray itemsArr) {
        ArrayList<YTVideoModel> list = new ArrayList<>();
        try {
            if ( itemsArr != null && itemsArr.length() > 0 ) {
                for (int i = 0; i < itemsArr.length(); i++) {
                    JSONObject item = itemsArr.getJSONObject(i);
                    if (!item.getString("snippet").equals("")) {
                        YTVideoModel model = new YTVideoModel();
                        model.setYtType(YTType.VIDEO);
                        JSONObject snippet = item.getJSONObject("snippet");
                        JSONObject statisticsObj = item.optJSONObject("statistics");
                        model.setVideoCount(item.optInt("video_count"));
                        if (statisticsObj != null) {
                            YTVideoStatistics statistics = new YTVideoStatistics();
                            statistics.setCommentCount(statisticsObj.optString("commentCount"));
                            statistics.setViewCount(statisticsObj.optString("viewCount"));
                            statistics.setFavoriteCount(statisticsObj.optString("favoriteCount"));
                            statistics.setDislikeCount(statisticsObj.optString("dislikeCount"));
                            statistics.setLikeCount(statisticsObj.optString("likeCount"));
                            model.setStatistics(statistics);
                        }
                        model.setPublishedAt(snippet.optString("publishedAt"));
                        model.setTitle(snippet.optString("title"));
                        model.setChannelId(snippet.optString("channelId"));
                        model.setChannelTitle(snippet.optString("channelTitle"));
                        model.setDescription(snippet.optString("description"));
                        JSONObject resourceId = snippet.getJSONObject("resourceId");
                        model.setVideoId(resourceId.optString("videoId"));
                        JSONObject thumbnails = snippet.optJSONObject("thumbnails");
                        if (thumbnails != null) {
                            JSONObject medium = thumbnails.getJSONObject("medium");
                            model.setImage(medium.optString("url"));
                        }
                        list.add(model);
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return list ;
    }

    public static YTVideoModel parsePlayListItems(String response) {
        YTVideoModel jsonModel = new YTVideoModel();
        try {
            JSONObject jsonObject = new JSONObject(response);
            JSONArray itemsArr = jsonObject.getJSONArray("video_list");
            jsonModel.setLoadMore( (  itemsArr.length() >= jsonObject.optInt( "video_count_page_limit" , 0 ) ) );
            jsonModel.setList(parsePlayListItemsJsonArray(itemsArr));
        } catch (JSONException e) {
            e.printStackTrace();
            jsonModel.setError(YTConstant.NO_DATA);
        }
        return jsonModel;
    }
    public static YTVideoStatistics parseStatistics(String response) {
        YTVideoStatistics jsonModel = new YTVideoStatistics();
        try {
            jsonModel.setList(new ArrayList<YTVideoStatistics>());
            JSONObject mainObject = new JSONObject(response);
            if (!mainObject.getString("items").equals("")) {
                JSONArray itemsArr = mainObject.getJSONArray("items");
                for (int i = 0; i < itemsArr.length(); i++) {
                    YTVideoStatistics model = new YTVideoStatistics();
                    JSONObject item = itemsArr.getJSONObject(i);
                    model.setVideoId(item.optString("id"));
                    JSONObject snippet = item.getJSONObject("snippet");
                    model.setPublishedAt(snippet.optString("publishedAt"));
                    model.setChannelId(snippet.optString("channelId"));
                    model.setChannelTitle(snippet.optString("channelTitle"));
                    model.setTitle(snippet.optString("title"));
                    model.setDescription(snippet.optString("description"));
                    JSONObject statistics = item.getJSONObject("statistics");
                    model.setViewCount(statistics.optString("viewCount"));
                    model.setLikeCount(statistics.optString("likeCount"));
                    model.setDislikeCount(statistics.optString("dislikeCount"));
                    model.setFavoriteCount(statistics.optString("favoriteCount"));
                    model.setCommentCount(statistics.optString("commentCount"));
                    JSONObject contentDetails = item.getJSONObject("contentDetails");
                    model.setDuration(YTUtility.getValidDuration(contentDetails.optString("duration")));
                    jsonModel.getList().add(model);
                }
            } else if (!mainObject.getString("error").equals("")) {
                JSONObject errorObject = mainObject.getJSONObject("error");
                jsonModel.setError(errorObject.getString("message"));
            }
        } catch (JSONException e) {
            e.printStackTrace();
            jsonModel.setError(YTConstant.NO_DATA);
        }
        return jsonModel;
    }
}
