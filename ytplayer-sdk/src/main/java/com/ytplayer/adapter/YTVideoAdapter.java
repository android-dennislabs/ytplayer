package com.ytplayer.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.helper.task.TaskRunner;
import com.squareup.picasso.Picasso;
import com.ytplayer.R;
import com.ytplayer.YTPlayer;
import com.ytplayer.task.YTInsertPlayListTask;
import com.ytplayer.util.YTConstant;
import com.ytplayer.util.YTDbHelper;
import com.ytplayer.util.YTType;
import com.ytplayer.views.YTPlayerTextView;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;
import java.util.concurrent.Callable;


/**
 * @author Created by Abhijit on 25/06/2018.
 */
public class YTVideoAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final String TAG = YTVideoAdapter.class.getSimpleName();
    private final OnItemClickListener<YTVideoModel> listener;
    private int minimumItemCount = YTConstant.MIN_RESULTS_COUNT;
    private boolean isLoading;
    private final Context context;
    private YTType ytType;
    private ArrayList<YTVideoModel> youtubeVideoModelArrayList;
    private LoadMoreItems onLoadMoreListener;
    private int catId = 0;
    private boolean isShowBookmark = false;

    public void setShowBookmark(boolean showBookmark) {
        isShowBookmark = showBookmark;
    }

    public YTVideoAdapter setMinimumItemCount(int minimumItemCount) {
        this.minimumItemCount = minimumItemCount;
        return this;
    }

    public YTVideoAdapter setOnLoadMoreListener(LoadMoreItems mOnLoadMoreListener) {
        this.onLoadMoreListener = mOnLoadMoreListener;
        return this;
    }

    public interface LoadMoreItems {
        void onLoadMore();

        void onProgressUpdateBar(int visibility);
    }

    public YTVideoAdapter(Context context, YTType ytType, ArrayList<YTVideoModel> youtubeVideoModelArrayList, OnItemClickListener<YTVideoModel> listener) {
        this.context = context;
        this.ytType = ytType;
        this.youtubeVideoModelArrayList = youtubeVideoModelArrayList;
        this.listener = listener;
    }

    @Override
    @NonNull
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.yt_video_custom_layout2, parent, false);
        return new YoutubeViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, final int position) {
        if (viewHolder instanceof YoutubeViewHolder) {
            YoutubeViewHolder holder = (YoutubeViewHolder) viewHolder;
            final YTVideoModel item = youtubeVideoModelArrayList.get(position);

            if (!TextUtils.isEmpty(item.getTitle())) {
                holder.tvTitle.setVisibility(View.VISIBLE);
                holder.tvTitle.setText(item.getTitle());
            } else {
                holder.tvTitle.setVisibility(View.GONE);
            }
            if (ytType == YTType.PLAYLIST && !TextUtils.isEmpty(item.getChannelTitle())) {
                holder.tvChannel.setVisibility(View.VISIBLE);
                holder.tvChannel.setText(item.getChannelTitle());
            } else {
                holder.tvChannel.setVisibility(View.GONE);
            }
            if (item.getStatistics() != null && !TextUtils.isEmpty(item.getStatistics().getDuration())) {
                holder.tvDuration.setVisibility(View.VISIBLE);
                holder.llDuration.setVisibility(View.VISIBLE);
                holder.tvDuration.setText(item.getStatistics().getDuration());
            } else {
                holder.tvDuration.setVisibility(View.GONE);
                holder.llDuration.setVisibility(View.GONE);
            }
            if (item.getStatistics() != null && !TextUtils.isEmpty(item.getStatistics().getViewCount())) {
                holder.tvViews.setVisibility(View.VISIBLE);
                holder.tvViews.setText(getViewCount(item.getStatistics().getViewCount()) + " views");
            } else {
                holder.tvViews.setVisibility(View.GONE);
            }
            if (!TextUtils.isEmpty(item.getTotalResults()) && !item.getTotalResults().equals("0")) {
                holder.tvCount.setText(item.getTotalResults());
                holder.tvCount.setVisibility(View.VISIBLE);
                holder.llCount.setVisibility(View.VISIBLE);
            } else {
                holder.tvCount.setVisibility(View.GONE);
                holder.llCount.setVisibility(View.GONE);
            }

            if (item.getImage() == null) {
                holder.ivThumbnail.setVisibility(View.VISIBLE);
                holder.ivThumbnail.setImageResource(R.drawable.ic_yt_placeholder);
            } else {
                holder.ivThumbnail.setVisibility(View.VISIBLE);
                Picasso.get()
                        .load(item.getImage())
                        .placeholder(R.drawable.ic_yt_placeholder)
                        .fit().centerInside()
                        .into(holder.ivThumbnail);
            }

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null) {
                        listener.onItemUpdate(position);
                        listener.onItemClick(item, ytType);
                    }
                }
            });

            if (onLoadMoreListener != null && getItemCount() >= minimumItemCount
                    && !isLoading && position == youtubeVideoModelArrayList.size() - 1) {
                updateList(true);
                onLoadMoreListener.onLoadMore();
                isLoading = true;
            }
            holder.cardView.setCardBackgroundColor(ContextCompat.getColor(context, item.isRead() ? R.color.yt_color_video_watched : R.color.themeBackgroundCardColor));

            if(item.getVideoDuration() > 0) {
                holder.progressBar.setMax(item.getVideoDuration());
                holder.progressBar.setProgress(item.getVideoTime());
                holder.progressBar.setVisibility(View.VISIBLE);
            }else {
                holder.progressBar.setVisibility(View.GONE);
            }

            updateMenuIcon(holder.ivBookmark, item);
        }
    }

    private String getViewCount(String views) {
        try {
            return NumberFormat.getNumberInstance(Locale.US).format(Integer.parseInt(views));
        } catch (NumberFormatException e) {
            e.printStackTrace();
            return views;
        }
    }

    /**
     * Call this method when no data available on server or no when no longer to use.
     */
    public void stop() {
        isLoading = true;
        updateList(false);
    }

    private void updateList(boolean isAdd) {
        onLoadMoreListener.onProgressUpdateBar(isAdd ? View.VISIBLE : View.GONE);
    }

    /**
     * Call this method when data fetch successfully from server and hide progressBar.
     */
    public void finish() {
        updateList(false);
        isLoading = false;
    }

    public void setYtType(YTType ytType) {
        this.ytType = ytType;
    }

    @Override
    public int getItemCount() {
        return youtubeVideoModelArrayList != null ? youtubeVideoModelArrayList.size() : 0;
    }

    class YoutubeViewHolder extends RecyclerView.ViewHolder {

        private final ImageView ivThumbnail;
        private final View llDuration, llCount;
        private final CardView cardView;
        private final ProgressBar progressBar;
        private final ImageView ivBookmark;
        private TextView tvTitle, tvDuration, tvCount, tvViews, tvChannel;

        YoutubeViewHolder(View itemView) {
            super(itemView);
            cardView = itemView.findViewById(R.id.card_view);
            ivThumbnail = itemView.findViewById(R.id.imageView);
            tvTitle = itemView.findViewById(R.id.tv_video_title_label);
            tvChannel = itemView.findViewById(R.id.tv_title_channel);
            tvDuration = itemView.findViewById(R.id.tv_video_duration);
            llDuration = itemView.findViewById(R.id.ll_video_duration);
            tvCount = itemView.findViewById(R.id.tv_total_video_count);
            llCount = itemView.findViewById(R.id.ll_video_count);
            tvViews = itemView.findViewById(R.id.tv_video_views_count);
            progressBar = itemView.findViewById(R.id.progress_bar);
            ivBookmark = itemView.findViewById(R.id.iv_bookmark);

            YTPlayerTextView.applyFont(tvTitle, tvDuration, tvCount, tvViews, tvChannel);

            if(isShowBookmark) {
                ivBookmark.setVisibility(View.VISIBLE);
                ivBookmark.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(youtubeVideoModelArrayList.size() > 0 && getAbsoluteAdapterPosition() >= 0 && youtubeVideoModelArrayList.size() > getAbsoluteAdapterPosition()) {
                            makeFavourite(ivBookmark, youtubeVideoModelArrayList.get(getAbsoluteAdapterPosition()));
                        }
                    }
                });
            }else {
                ivBookmark.setVisibility(View.GONE);
            }
        }
    }

    public void setCatId(int catId) {
        this.catId = catId;
    }

    public void setLoading(boolean loading) {
        isLoading = loading;
    }

    private void makeFavourite(ImageView ivBookmark, YTVideoModel mItem) {
        if (mItem != null) {
            mItem.setIsFav(mItem.getIsFav() == YTDbHelper.ACTIVE ? YTDbHelper.IN_ACTIVE : YTDbHelper.ACTIVE);
            mItem.setYtType(YTType.PLAYLIST);
            new YTInsertPlayListTask(catId, mItem).execute();
            notifyDataSetChanged();
        }
    }

    private void updateMenuIcon(ImageView ivBookmark, YTVideoModel mItem) {
        if(isShowBookmark) {
            if (ivBookmark != null) {
                ivBookmark.setImageResource(mItem.getIsFav() == YTDbHelper.ACTIVE ? R.drawable.ic_yt_menu_bookmark_filled : R.drawable.ic_yt_menu_bookmark);
            }
        }
    }
}
