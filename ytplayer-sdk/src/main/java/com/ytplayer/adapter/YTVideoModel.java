package com.ytplayer.adapter;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

import com.helper.util.GsonParser;
import com.ytplayer.util.YTDbHelper;
import com.ytplayer.util.YTType;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;


/**
 * @author Created by Abhijit on 25/06/2018.
 */
public class YTVideoModel implements Parcelable, Cloneable {
    private String videoId, title, duration;
    private String nextPageToken, description, publishedAt;
    private String image;
    private String channelId;
    private String channelTitle;
    private String totalResults;
    private String error;
    private ArrayList<YTVideoModel> list;
    private List<String> videoIds;
    private YTVideoStatistics statistics;
    private int videoCount;
    private boolean isLoadMore;
    private String host;
    private int videoTime = 0;
    private YTType ytType = YTType.DEFAULT ;
    private int isRead = 0;
    private int isFav = 0;
    private String jsonData;
    private String videoTimeFormatted;
    private int videoDuration = 0;
    private int autoId;

    public int getAutoId() {
        return autoId;
    }

    public void setAutoId(int autoId) {
        this.autoId = autoId;
    }

    public int getVideoDuration() {
        return videoDuration;
    }

    public void setVideoDuration(int videoDuration) {
        this.videoDuration = videoDuration;
    }

    public String getVideoTimeFormatted() {
        return videoTimeFormatted;
    }

    public void setVideoTimeFormatted(String videoTimeFormatted) {
        this.videoTimeFormatted = videoTimeFormatted;
    }

    public boolean isRead() {
        return isRead == YTDbHelper.ACTIVE;
    }

    public int getIsRead() {
        return isRead;
    }

    public void setIsRead(int isRead) {
        this.isRead = isRead;
    }

    public int getIsFav() {
        return isFav;
    }

    public void setIsFav(int isFav) {
        this.isFav = isFav;
    }

    public String getJsonData() {
        return jsonData;
    }

    public void setJsonData(String jsonData) {
        this.jsonData = jsonData;
    }

    public int getVideoTime() {
        return videoTime;
    }

    public void setVideoTime(int videoTime) {
        this.videoTime = videoTime;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public YTType getYtType() {
        return ytType;
    }

    public void setYtType(YTType ytType) {
        this.ytType = ytType;
    }

    public boolean isLoadMore() {
        return isLoadMore;
    }

    public void setLoadMore(boolean loadMore) {
        isLoadMore = loadMore;
    }

    public YTVideoStatistics getStatistics() {
        return statistics;
    }

    public void setStatistics(YTVideoStatistics statistics) {
        this.statistics = statistics;
    }

    public List<String> getVideoIds() {
        return videoIds;
    }

    public void setVideoIds(List<String> videoIds) {
        this.videoIds = videoIds;
    }

    public String getChannelId() {
        return channelId;
    }


    public String getTotalResults() {
        return totalResults;
    }

    public void setTotalResults(String totalResults) {
        if (TextUtils.isEmpty(totalResults)) {
            this.totalResults = "0";
        }else {
            try {
                this.totalResults = String.format( Locale.ENGLISH , "%d" , (int)Float.parseFloat(totalResults) );
            } catch (NumberFormatException e) {
                this.totalResults = "0";
                e.printStackTrace();
            }
        }
    }

    public String getChannelTitle() {
        return channelTitle;
    }

    public void setChannelTitle(String channelTitle) {
        this.channelTitle = channelTitle;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public ArrayList<YTVideoModel> getList() {
        return list;
    }

    public void setList(ArrayList<YTVideoModel> list) {
        this.list = list;
    }

    public String getError() {
        return error;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public void setError(String error) {
        this.error = error;
    }

    public static YTVideoModel Builder() {
        return new YTVideoModel();
    }


    public String getNextPageToken() {
        return nextPageToken;
    }

    public YTVideoModel setNextPageToken(String nextPageToken) {
        this.nextPageToken = nextPageToken;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public YTVideoModel setDescription(String description) {
        this.description = description;
        return this;
    }

    public String getPublishedAt() {
        return publishedAt;
    }

    public YTVideoModel setPublishedAt(String publishedAt) {
        this.publishedAt = publishedAt;
        return this;
    }

    public String getVideoId() {
        return videoId;
    }

    public YTVideoModel setVideoId(String videoId) {
        this.videoId = videoId;
        return this;
    }

    public String getTitle() {
        return title;
    }

    public YTVideoModel setTitle(String title) {
        this.title = title;
        return this;
    }

    public int getVideoCount() {
        return videoCount;
    }

    public void setVideoCount(int videoCount) {
        this.videoCount = videoCount;
    }

    public YTVideoModel() {
    }

    public String getDuration() {
        return duration;
    }

    public YTVideoModel setDuration(String duration) {
        this.duration = duration;
        return this;
    }
    public String toJson() {
        return GsonParser.getGson().toJson(this, YTVideoModel.class);
    }


    protected YTVideoModel(Parcel in) {
        videoId = in.readString();
        title = in.readString();
        duration = in.readString();
        nextPageToken = in.readString();
        description = in.readString();
        publishedAt = in.readString();
        image = in.readString();
        channelId = in.readString();
        channelTitle = in.readString();
        totalResults = in.readString();
        error = in.readString();
        list = in.createTypedArrayList(YTVideoModel.CREATOR);
        videoIds = in.createStringArrayList();
        statistics = in.readParcelable(YTVideoStatistics.class.getClassLoader());
        videoCount = in.readInt();
        isLoadMore = in.readByte() != 0;
        host = in.readString();
        videoTime = in.readInt();
        isRead = in.readInt();
        isFav = in.readInt();
        jsonData = in.readString();
        videoTimeFormatted = in.readString();
        videoDuration = in.readInt();
        autoId = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(videoId);
        dest.writeString(title);
        dest.writeString(duration);
        dest.writeString(nextPageToken);
        dest.writeString(description);
        dest.writeString(publishedAt);
        dest.writeString(image);
        dest.writeString(channelId);
        dest.writeString(channelTitle);
        dest.writeString(totalResults);
        dest.writeString(error);
        dest.writeTypedList(list);
        dest.writeStringList(videoIds);
        dest.writeParcelable(statistics, flags);
        dest.writeInt(videoCount);
        dest.writeByte((byte) (isLoadMore ? 1 : 0));
        dest.writeString(host);
        dest.writeInt(videoTime);
        dest.writeInt(isRead);
        dest.writeInt(isFav);
        dest.writeString(jsonData);
        dest.writeString(videoTimeFormatted);
        dest.writeInt(videoDuration);
        dest.writeInt(autoId);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<YTVideoModel> CREATOR = new Creator<YTVideoModel>() {
        @Override
        public YTVideoModel createFromParcel(Parcel in) {
            return new YTVideoModel(in);
        }

        @Override
        public YTVideoModel[] newArray(int size) {
            return new YTVideoModel[size];
        }
    };


    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    public YTVideoModel getClone() {
        try {
            return (YTVideoModel) clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
            return new YTVideoModel();
        }
    }
}
