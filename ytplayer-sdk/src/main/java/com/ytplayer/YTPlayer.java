package com.ytplayer;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;

import com.config.config.ConfigManager;
import com.ytplayer.adapter.YTVideoModel;
import com.ytplayer.base.YTPlayerClass;
import com.ytplayer.util.YTDbHelper;
import com.ytplayer.util.YTListener;

import java.util.ArrayList;

/**
 * @author Created by Abhijit on 2/6/2018.
 */
public interface YTPlayer {

    enum VideoType {
        OPEN_INTERNAL_PLAYER,
        OPEN_INTERNAL_SLIDING_PLAYER,
        OPEN_EXTERNAL
    }

    static YTPlayer getInstance(Context context, String googleApiKey) {
        return YTPlayerClass.Builder(context, googleApiKey);
    }

    static YTPlayer getInstance() throws Exception {
        return YTPlayerClass.Builder();
    }

    YTPlayer setConfigManager(ConfigManager configManager);

    ConfigManager getConfigManager();

    void openVideo(String videoId);

    /**
     * Used for In app open
     */
    void openVideo(String videoId, boolean isDetailVisible);

    /**
     * Used for External app open
     */
    void openVideo(Activity activity, String videoId, boolean isDetailVisible);

    Typeface getTypeface();

    YTPlayer setTypeface(Typeface typeface);

    YTPlayer maxListItemsCount(int maxResultsCount);

    YTPlayer setPlayerType(VideoType playerType);

    void openPlaylistExternal(Activity activity, String playlistId);

    void openViewPlaylist(ArrayList<YTVideoModel> playlist);

    void openViewPlaylist(String playerName, ArrayList<YTVideoModel> playlist);

    void openPlaylist(String host, String playerName, String playListId);

    /**
     * @param playerName Toolbar title name
     * @param json       contains playlist ids , videos ids and mix ranking for both
     */
    void openYtByPlayListVideos(String playerName, String host, String json);

    void openPlaylistMultipleIds(String playerName, String host, String playListIds);

    /**
     * @param catId       subject category id
     * @param playerName  Toolbar title name
     * @param playListIds Playlist ids in comma separated
     */
    void openPlaylistMultipleIds(int catId, String playerName, String host, String playListIds);

    void openBookmarkChannels(String title);

    void openBookmarkVideos(String title);

    void openSinglePlayVideoActivity(YTVideoModel videoModel);


    void openChannel(String playerName, String channelId);

//    void openSearch(String youtubeChannelId) {
//        YTUtility.openInternalYoutubePlaylistPlayer(context, youtubeChannelId);
//    }

    void openSubscribeOption(String youtubeChannelId);

    YTPlayer addHistoryListener(YTListener.OnHistoryListener callback);

    YTListener.OnHistoryListener getHistoryListener();

    void removeHistoryListener();

    void dispatchHistoryUpdated(YTVideoModel model);

    void insertHistory(YTVideoModel model);

    Context getContext();

    YTDbHelper getDBObject();

    boolean isMarkFavouriteDisable();

    YTPlayer setMarkFavouriteDisable(boolean markFavouriteDisable);

    boolean isMarkBookmarkDisable();

    YTPlayer setMarkBookmarkDisable(boolean markBookmarkDisable);
}
