package com.ytplayer;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.content.ContextCompat;

import com.config.util.ConfigUtil;
import com.helper.util.BaseConstants;
import com.helper.util.BaseUtil;
import com.ytplayer.activity.playlist.YTChannelPlaylistActivity;
import com.ytplayer.activity.playlist.YTPlaySingleVideoActivity;
import com.ytplayer.activity.playlist.YTPlaylistActivity;
import com.ytplayer.activity.playlist.YTPlaylistVideoActivity;
import com.ytplayer.activity.single.YTSlidingActivity;
import com.ytplayer.adapter.YTVideoModel;
import com.ytplayer.util.Logger;
import com.ytplayer.util.YTConfig;
import com.ytplayer.util.YTConstant;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

/**
 * @author Created by Abhijit on 2/6/2018.
 */
public class YTUtility {

    /**
     * @param videoId: youtube video id
     * Image extensions are: mqdefault.jpg, 0.jpg, hqdefault.jpg, default.jpg, sddefault.jpg, maxresdefault.jpg
     * Also available on http://img.youtube.com/vi/
     */
    public static String getYoutubePlaceholderImage(String videoId) {
        return "https://i.ytimg.com/vi/"+ videoId +"/mqdefault.jpg";
    }

//    public static void showKeyboard(View view, Context activity) {
//        if (view.requestFocus()) {
//            InputMethodManager imm = (InputMethodManager)
//                    activity.getSystemService(Context.INPUT_METHOD_SERVICE);
//            if (imm != null) {
////                imm.showSoftInput(view, InputMethodManager.SHOW_FORCED);
//                imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
//            }
//        }
//    }

    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        View f = activity.getCurrentFocus();
        if (null != f && null != f.getWindowToken() && EditText.class.isAssignableFrom(f.getClass()))
            imm.hideSoftInputFromWindow(f.getWindowToken(), 0);
        else
            activity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }

    public static void openExternalYoutubeVideoPlayer(Activity activity, String googleApiKey, String youtubeVideoId) {
        try {
//            activity.startActivity(YouTubeStandalonePlayer.createVideoIntent(activity, googleApiKey, youtubeVideoId, 0, true, false));
        } catch (Exception e) {
            e.printStackTrace();
            Logger.log(e.toString());
        }
    }

    public static void openExternalYoutubePlaylistPlayer(Activity activity, String googleApiKey, String playlistId) {
        try {
//            activity.startActivity(YouTubeStandalonePlayer.createPlaylistIntent(activity, googleApiKey, playlistId, 0, 0, true, true));
        } catch (Exception e) {
            e.printStackTrace();
            Logger.log(e.toString());
        }
    }

    public static void openYoutubeApp(Activity activity, String videoId) {
        try {
            Intent appIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("vnd.youtube:" + videoId));
            Intent webIntent = new Intent(Intent.ACTION_VIEW,
                    Uri.parse("http://www.youtube.com/watch?v=" + videoId));
            try {
                activity.startActivity(appIntent);
            } catch (ActivityNotFoundException ex) {
                activity.startActivity(webIntent);
            }
        } catch (Exception e) {
            e.printStackTrace();
            Logger.log(e.toString());
        }
    }

    public static void openInternalYoutubePlayer(Context context) {
        /*if(isValidInitialization(context)) {
            try {
                context.startActivity(new Intent(context, YTPlayerActivity.class)
                        .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
            } catch (Exception e) {
                e.printStackTrace();
                Logger.log(e.toString());
            }
        }*/
    }

    public static void openInternalYoutubePlayer(Context context, boolean isDetailVisible) {
        if(isValidInitialization(context)) {
            try {
                Intent intent = new Intent(context, YTSlidingActivity.class);
                intent.putExtra(YTConstant.VIDEO_DETAIL, isDetailVisible);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            } catch (Exception e) {
                e.printStackTrace();
                Logger.log(e.toString());
            }
        }
    }

    public static void openInternalYoutubeSlidingPanel(Context context) {
        if(isValidInitialization(context)) {
            try {
                context.startActivity(new Intent(context, YTSlidingActivity.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
            } catch (Exception e) {
                e.printStackTrace();
                Logger.log(e.toString());
            }
        }
    }

    public static void openInternalYoutubePlaylistPlayer(Context context, String playerName, ArrayList<YTVideoModel> playlist) {
        if(isValidInitialization(context)) {
            try {
                Intent intent = new Intent(context, YTPlaylistActivity.class);
                intent.putExtra(YTConstant.PLAYER_NAME, playerName);
                intent.putExtra(YTConstant.PLAYLIST, playlist);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            } catch (Exception e) {
                e.printStackTrace();
                Logger.log(e.toString());
            }
        }
    }

    public static void openInternalYoutubePlaylistPlayer(Context context, String host, String playerName , int position , ArrayList<YTVideoModel> ytVideoModels) {
        if(isValidInitialization(context)) {
            try {
                Intent intent = new Intent(context, YTPlaylistActivity.class);
                intent.putExtra(YTConstant.API_HOST, host);
                intent.putExtra(YTConstant.PLAYER_NAME, playerName);
                intent.putExtra(YTConstant.POSITION, position);
                intent.putExtra(YTConstant.PLAYLIST, ytVideoModels);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            } catch (Exception e) {
                e.printStackTrace();
                Logger.log(e.toString());
            }
        }
    }

    public static void openInternalYoutubeByPlaylistId(Context context, String host, String playerName, String playListId) {
        if(isValidInitialization(context)) {
            try {
                Intent intent = new Intent(context, YTPlaylistActivity.class);
                intent.putExtra(YTConstant.API_HOST, host);
                intent.putExtra(YTConstant.PLAYER_NAME, playerName);
                intent.putExtra(YTConstant.PLAYLIST_ID, playListId);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            } catch (Exception e) {
                e.printStackTrace();
                Logger.log(e.toString());
            }
        }
    }

    public static void openInternalYoutubeByChannelId(Context context, String playerName, String channelId) {
        if(isValidInitialization(context)) {
            try {
                Intent intent = new Intent(context, YTChannelPlaylistActivity.class);
                intent.putExtra(YTConstant.PLAYER_NAME, playerName);
                intent.putExtra(YTConstant.CHANNEL_ID, channelId);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            } catch (Exception e) {
                e.printStackTrace();
                Logger.log(e.toString());
            }
        }
    }

    /**
     * @param host
     * @param playerName  Toolbar title name
     * @param json contains playlist ids , videos ids and mix ranking for both
     */
    public static void openInternalYoutubeByPlayListVideos(Context context, String host, String playerName, String json) {
        if(isValidInitialization(context)) {
            try {
                Intent intent = new Intent(context, YTPlaylistVideoActivity.class);
                intent.putExtra(YTConstant.API_HOST, host);
                intent.putExtra(YTConstant.PLAYER_NAME, playerName);
                intent.putExtra(YTConstant.DATA, json);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            } catch (Exception e) {
                e.printStackTrace();
                Logger.log(e.toString());
            }
        }
    }

    public static void openYoutubePlaySingleVideoActivity(Context context, YTVideoModel item) {
        if(isValidInitialization(context)) {
            try {
                Intent intent = new Intent(context, YTPlaySingleVideoActivity.class);
                intent.putExtra(YTConstant.EXTRA_PROPERTY, item);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            } catch (Exception e) {
                e.printStackTrace();
                Logger.log(e.toString());
            }
        }
    }

    public static void openInternalYoutubeByPlayListMultipleIds(Context context, String host, String playerName, String playListIds) {
        openInternalYoutubeByPlayListMultipleIds(context, 0, host, playerName, playListIds);
    }
    /**
     * @param catId category id
     * @param host config host name
     * @param playerName  Toolbar title name
     * @param playListIds Playlist ids in comma separated
     */
    public static void openInternalYoutubeByPlayListMultipleIds(Context context, int catId, String host, String playerName, String playListIds) {
        if(isValidInitialization(context)) {
            try {
                Intent intent = new Intent(context, YTChannelPlaylistActivity.class);
                intent.putExtra(YTConstant.CAT_ID, catId);
                intent.putExtra(YTConstant.API_HOST, host);
                intent.putExtra(YTConstant.PLAYER_NAME, playerName);
                intent.putExtra(YTConstant.PLAYLIST_ID, playListIds);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            } catch (Exception e) {
                e.printStackTrace();
                Logger.log(e.toString());
            }
        }
    }

    public static void openBookmarkChannels(Context context, String title) {
        if(isValidInitialization(context)) {
            openBookmarkChannels(context, 0, title);
        }
    }

    private static boolean isValidInitialization(Context context) {
        if(TextUtils.isEmpty(YTConfig.getApiKey())){
            BaseUtil.showToast(context, BaseConstants.Error.MSG_ERROR);
            return false;
        }
        return true;
    }

    public static void openBookmarkChannels(Context context, int catId, String title) {
        if(isValidInitialization(context)) {
            try {
                Intent intent = new Intent(context, YTChannelPlaylistActivity.class);
                intent.putExtra(YTConstant.CAT_ID, catId);
                intent.putExtra(YTConstant.PLAYER_NAME, title);
                intent.putExtra(YTConstant.IS_SHOW_BOOKMARK, true);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            } catch (Exception e) {
                e.printStackTrace();
                Logger.log(e.toString());
            }
        }
    }
    public static void openBookmarkVideos(Context context, String title) {
        if(isValidInitialization(context)) {
            try {
                Intent intent = new Intent(context, YTPlaylistActivity.class);
                intent.putExtra(YTConstant.PLAYER_NAME, title);
                intent.putExtra(YTConstant.IS_SHOW_BOOKMARK, true);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            } catch (Exception e) {
                e.printStackTrace();
                Logger.log(e.toString());
            }
        }
    }

//    public static void openInternalYoutubePlaylistPlayer(Context context, String channelId) {
//        if(isValidInitialization(context)) {
//            try {
//                Intent intent = new Intent(context, YTSearchActivity.class);
//                intent.putExtra(YTConstant.CHANNEL_ID, channelId);
//                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                context.startActivity(intent);
//            } catch (Exception e) {
//                e.printStackTrace();
//                Logger.log(e.toString());
//            }
//        }
//    }


    public static int parseInt(String totalResults) {
        try {
            return Integer.parseInt(totalResults);
        } catch (NumberFormatException e) {
            e.printStackTrace();
            return 0;
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    public static void log(String message) {
        if (YTConstant.isLogEnabled) {
            Log.d("okhttp", message);
        }
    }

    public static void showNoData(View view, int textVisibility) {
        showNoData(view, textVisibility, null);
    }

    public static void showNoData(View view, int textVisibility, String message) {
        if (view != null) {
            view.setVisibility(textVisibility);
            if (textVisibility == View.VISIBLE) {
                TextView tvNoData = view.findViewById(R.id.tv_no_data);
                if (view.findViewById(R.id.player_progressbar) != null) {
                    view.findViewById(R.id.player_progressbar).setVisibility(View.GONE);
                }
                if (tvNoData != null) {
                    tvNoData.setVisibility(textVisibility);
                    if (ConfigUtil.isConnected(view.getContext())) {
                        tvNoData.setText(message == null ? YTConstant.NO_DATA : message);
                    } else {
                        tvNoData.setText(YTConstant.NO_INTERNET_CONNECTION);
                    }
                }
            }
        }
    }

    // input date is PT1H1M13S
    public static String getValidDuration(String youtubeDuration) {
        Calendar c = new GregorianCalendar();
        try {
            DateFormat df = new SimpleDateFormat("'PT'mm'M'ss'S'", Locale.US);
            Date d = df.parse(youtubeDuration);
            c.setTime(d);
        } catch (ParseException e) {
            try {
                DateFormat df = new SimpleDateFormat("'PT'hh'H'mm'M'ss'S'", Locale.US);
                Date d = df.parse(youtubeDuration);
                c.setTime(d);
            } catch (ParseException e1) {
                try {
                    DateFormat df = new SimpleDateFormat("'PT'ss'S'", Locale.US);
                    Date d = df.parse(youtubeDuration);
                    c.setTime(d);
                } catch (ParseException e2) {
                }
            }
        }
        c.setTimeZone(TimeZone.getDefault());

        String time = "";
        if (c.get(Calendar.HOUR) > 0) {
            if (String.valueOf(c.get(Calendar.HOUR)).length() == 1) {
                time += "0" + c.get(Calendar.HOUR);
            } else {
                time += c.get(Calendar.HOUR);
            }
            time += ":";
        }
        // test minute
        if (String.valueOf(c.get(Calendar.MINUTE)).length() == 1) {
            time += "0" + c.get(Calendar.MINUTE);
        } else {
            time += c.get(Calendar.MINUTE);
        }
        time += ":";
        // test second
        if (String.valueOf(c.get(Calendar.SECOND)).length() == 1) {
            time += "0" + c.get(Calendar.SECOND);
        } else {
            time += c.get(Calendar.SECOND);
        }
        return time;
    }

    public static void openInternalYoutubeSubscribe(Context context, String youtubeChannelId) {
        // Contact Developer to update this option
        // UnComment SubscribeActivity for use working Class
//        context.startActivity(new Intent(context, SubscribeActivity.class)
//                .putExtra(YTConstant.CHANNEL_ID, youtubeChannelId));
        // uncomment Core Library on gradle file
        Toast.makeText(context, "Contact Developer to update this option", Toast.LENGTH_SHORT).show();
    }

    public static void setTranslucentColor(Window window) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                window.setStatusBarColor(ContextCompat.getColor(window.getContext(), R.color.ytStatusBarColor));
                window.setNavigationBarColor(Color.BLACK);
            }

        }
    }

    public static String convertTime(int millis){
        String hms = String.format("%02d min : %02d sec",
                TimeUnit.MILLISECONDS.toMinutes(millis),
                TimeUnit.MILLISECONDS.toSeconds(millis) % TimeUnit.MINUTES.toSeconds(1));
        return hms;
    }

    public static String[][] splitArray(String[] arrayToSplit, int chunkSize){
        if(chunkSize<=0){
            return null;  // just in case :)
        }
        // first we have to check if the array can be split in multiple
        // arrays of equal 'chunk' size
        int rest = arrayToSplit.length % chunkSize;  // if rest>0 then our last array will have less elements than the others
        // then we check in how many arrays we can split our input array
        int chunks = arrayToSplit.length / chunkSize + (rest > 0 ? 1 : 0); // we may have to add an additional array for the 'rest'
        // now we know how many arrays we need and create our result array
        String[][] arrays = new String[chunks][];
        // we create our resulting arrays by copying the corresponding
        // part from the input array. If we have a rest (rest>0), then
        // the last array will have less elements than the others. This
        // needs to be handled separately, so we iterate 1 times less.
        for(int i = 0; i < (rest > 0 ? chunks - 1 : chunks); i++){
            // this copies 'chunk' times 'chunkSize' elements into a new array
            arrays[i] = Arrays.copyOfRange(arrayToSplit, i * chunkSize, i * chunkSize + chunkSize);
        }
        if(rest > 0){ // only when we have a rest
            // we copy the remaining elements into the last chunk
            arrays[chunks - 1] = Arrays.copyOfRange(arrayToSplit, (chunks - 1) * chunkSize, (chunks - 1) * chunkSize + rest);
        }
        return arrays;
    }
}
