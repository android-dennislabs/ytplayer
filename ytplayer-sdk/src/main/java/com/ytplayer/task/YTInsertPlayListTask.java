package com.ytplayer.task;

import com.helper.task.TaskRunner;
import com.ytplayer.YTPlayer;
import com.ytplayer.adapter.YTVideoModel;
import com.ytplayer.util.YTDbHelper;

import java.util.concurrent.Callable;

public class YTInsertPlayListTask {
    private final YTVideoModel mVideoModel;
    private final int catId;

    public YTInsertPlayListTask(YTVideoModel mVideoModel) {
        this.catId = 0;
        this.mVideoModel = mVideoModel;
    }
    public YTInsertPlayListTask(int catId, YTVideoModel mVideoModel) {
        this.catId = catId;
        this.mVideoModel = mVideoModel;
    }

    public void execute() {
        TaskRunner.getInstance().executeAsync(new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                YTDbHelper dbHelper = YTPlayer.getInstance().getDBObject();
                if (dbHelper != null) {
                    dbHelper.callDBFunction(new Callable<Void>() {
                        @Override
                        public Void call() throws Exception {
                            dbHelper.insertPlayList(catId, mVideoModel);
                            return null;
                        }
                    });
                }
                return null;
            }
        });
    }
}
