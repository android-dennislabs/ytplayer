package com.ytplayer.task;

import com.helper.callback.Response;
import com.helper.task.TaskRunner;
import com.ytplayer.YTPlayer;
import com.ytplayer.adapter.YTVideoModel;
import com.ytplayer.util.YTDbHelper;

import java.util.HashMap;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;

public class YTGetWatchListTask {
    private final List<YTVideoModel>  mVideoList;

    public YTGetWatchListTask(List<YTVideoModel> mVideoList) {
        this.mVideoList = mVideoList;
    }

    public void execute(String channelId, Response.Status<Boolean> callback) {
        TaskRunner.getInstance().executeAsync(new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                updateList(channelId, mVideoList);
                return null;
            }
        }, new TaskRunner.Callback<Void>() {
            @Override
            public void onComplete(Void result) {
                callback.onSuccess(true);
            }
        });
    }

    public static void updateList(String channelId, List<YTVideoModel> ytVideoList) {
        try {
            YTDbHelper dbHelper = YTPlayer.getInstance().getDBObject();
            dbHelper.callDBFunction(new Callable<Void>() {
                @Override
                public Void call() throws Exception {
                    HashMap<String, YTVideoModel> watchList = dbHelper.fetchWatchList(channelId);
                    if (ytVideoList != null && watchList.size() > 0) {
                        for (YTVideoModel item : ytVideoList){
                            YTVideoModel watchedItem = watchList.get(item.getVideoId());
                            if (watchedItem != null) {
                                updateModel(item, watchedItem);
                            }
                        }
                    }
                    return null;
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void updateModel(YTVideoModel item, YTVideoModel watchedItem) {
        item.setVideoTime(watchedItem.getVideoTime());
        item.setVideoDuration(watchedItem.getVideoDuration());
//        item.setVideoTimeFormatted(convertTime(watchedItem.getVideoTime()));
        item.setIsRead(YTDbHelper.ACTIVE);
    }

    private static String convertTime(int millis){
        String hms = String.format("%02d min : %02d sec",
                TimeUnit.MILLISECONDS.toMinutes(millis),
                TimeUnit.MILLISECONDS.toSeconds(millis) % TimeUnit.MINUTES.toSeconds(1));
        return hms;
    }
}
