package com.ytplayer.task;

import com.helper.task.TaskRunner;
import com.ytplayer.YTPlayer;
import com.ytplayer.adapter.YTVideoModel;
import com.ytplayer.util.YTDbHelper;

import java.util.concurrent.Callable;

public class YTInsertWatchListTask {
    private final YTVideoModel mVideoModel;

    public YTInsertWatchListTask(YTVideoModel mVideoModel) {
        this.mVideoModel = mVideoModel;
    }

    public void execute() {
        TaskRunner.getInstance().executeAsync(new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                YTDbHelper dbHelper = YTPlayer.getInstance().getDBObject();
                if (dbHelper != null) {
                    dbHelper.callDBFunction(new Callable<Void>() {
                        @Override
                        public Void call() throws Exception {
                            dbHelper.insertVideoInWatchList(mVideoModel);
                            return null;
                        }
                    });
                }
                return null;
            }
        });
    }
}
