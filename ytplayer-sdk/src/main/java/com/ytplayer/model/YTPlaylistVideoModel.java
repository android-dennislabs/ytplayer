package com.ytplayer.model;
import android.text.TextUtils;

import com.ytplayer.util.YTConstant;
import com.ytplayer.util.YTType;

import java.io.Serializable;

public class YTPlaylistVideoModel implements Serializable {

    private String id;

    private String type;

    private int rank;

    public YTType getYtType() {
        if (!TextUtils.isEmpty(type)){
            if ( type.equalsIgnoreCase(YTConstant.PLAYLIST) )
                return YTType.PLAYLIST;
            else if ( type.equalsIgnoreCase(YTConstant.VIDEO) )
                return YTType.VIDEO;
        }

        return YTType.DEFAULT;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getRank() {
        return rank;
    }

    public void setRank(int rank) {
        this.rank = rank;
    }

}