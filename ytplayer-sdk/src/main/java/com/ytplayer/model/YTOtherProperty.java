package com.ytplayer.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class YTOtherProperty implements Serializable {
    private String video_playlist_ids;
    private String video_ids;
    @SerializedName("item_list")
    @Expose
    private List<YTPlaylistVideoModel> ytPlaylistVideoModels;

    public String getVideo_playlist_ids() {
        return video_playlist_ids;
    }

    public void setVideo_playlist_ids(String video_playlist_ids) {
        this.video_playlist_ids = video_playlist_ids;
    }

    public String getVideo_ids() {
        return video_ids;
    }

    public void setVideo_ids(String video_ids) {
        this.video_ids = video_ids;
    }

    public List<YTPlaylistVideoModel> getYtPlaylistVideoModels() {
        return ytPlaylistVideoModels;
    }

    public void setYtPlaylistVideoModels(List<YTPlaylistVideoModel> ytPlaylistVideoModels) {
        this.ytPlaylistVideoModels = ytPlaylistVideoModels;
    }
}
