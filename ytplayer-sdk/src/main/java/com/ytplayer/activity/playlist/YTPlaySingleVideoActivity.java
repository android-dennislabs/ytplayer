package com.ytplayer.activity.playlist;

import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.helper.util.BaseConstants;
import com.helper.util.BaseUtil;
import com.ytplayer.slidinguppanel.SlidingUpPanelLayout;
import com.ytplayer.R;
import com.ytplayer.YTUtility;
import com.ytplayer.activity.YTBaseActivity;
import com.ytplayer.adapter.OnItemClickListener;
import com.ytplayer.adapter.YTVideoAdapter;
import com.ytplayer.adapter.YTVideoModel;
import com.ytplayer.network.ApiCall;
import com.ytplayer.network.JsonParser;
import com.ytplayer.network.ParamBuilder;
import com.ytplayer.network.YTNetwork;
import com.ytplayer.util.YTConstant;
import com.ytplayer.util.YTPlayList;
import com.ytplayer.util.YTType;

import java.util.ArrayList;

/**
 * @author Created by Abhijit on 2/6/2018.
 */
public class YTPlaySingleVideoActivity extends YTBaseActivity{

    private String playerName;
    private String host;
    private YTVideoModel videoModel;

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        if (playerName != null) {
            outState.putString(YTConstant.PLAYER_NAME, playerName);
        }
        if (host != null) {
            outState.putString(YTConstant.API_HOST, host);
        }
        if (videoModel != null) {
            outState.putParcelable(YTConstant.EXTRA_PROPERTY, videoModel);
        }

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        YTUtility.setTranslucentColor(getWindow());
        isSinglePlayVideo = true;
        super.onCreate(savedInstanceState);
        setContentView(R.layout.yt_activity_youtube_single_video);
        init();
        if (savedInstanceState == null) {
            getBundle(getIntent());
        } else {
            host = savedInstanceState.getString(YTConstant.API_HOST);
            playerName = savedInstanceState.getString(YTConstant.PLAYER_NAME);
            videoModel = savedInstanceState.getParcelable(YTConstant.EXTRA_PROPERTY);
        }
        setupToolBar();
    }


    private void getBundle(Intent intent) {
        try {
            videoModel = intent.getParcelableExtra(YTConstant.EXTRA_PROPERTY);
            if (videoModel != null) {
                host = intent.getStringExtra(YTConstant.API_HOST);
                playerName = videoModel.getTitle();
                playVideo(videoModel);
            } else {
                BaseUtil.showToast(this, YTConstant.INVALID_PROPERTY);
                finish();
            }
        } catch (Exception e) {
            BaseUtil.showToast(this, YTConstant.INVALID_PROPERTY);
            finish();
        }
    }


    private void setupToolBar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (!TextUtils.isEmpty(playerName)) {
            getSupportActionBar().setTitle(playerName);
        }
    }


    @Override
    public void initYTPlayer() {
        dragView = findViewById(R.id.dragView);
//        playerView = findViewById(R.id.youtubePlayerView);
        scrollView = findViewById(R.id.scroll_view_video_detail);
    }

//    @Override
//    public void onInitializationSuccess() {
//
//    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.yt_menu_video_play, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            this.finish();
            return true;
        } else if (id == R.id.menu_item_video_youtube) {
            openYoutubeApp();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onPanelStateChanged(SlidingUpPanelLayout.PanelState newState) {

    }

    @Override
    public void onUpdateList(YTVideoModel item) {

    }

}