package com.ytplayer.activity.playlist;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.adssdk.AdsSDK;
import com.adssdk.PageAdsAppCompactActivity;
import com.config.util.ConfigUtil;
import com.helper.callback.NetworkListener;
import com.helper.task.TaskRunner;
import com.helper.util.BaseConstants;
import com.helper.util.BaseUtil;
import com.ytplayer.R;
import com.ytplayer.YTPlayer;
import com.ytplayer.YTUtility;
import com.ytplayer.adapter.OnItemClickListener;
import com.ytplayer.adapter.YTVideoAdapter;
import com.ytplayer.adapter.YTVideoModel;
import com.ytplayer.network.ApiCall;
import com.ytplayer.network.JsonParser;
import com.ytplayer.network.ParamBuilder;
import com.ytplayer.network.YTNetwork;
import com.ytplayer.util.YTConstant;
import com.ytplayer.util.YTDbHelper;
import com.ytplayer.util.YTType;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.Callable;

/**
 * @author Created by Abhijit on 2/6/2018.
 */
public class YTChannelPlaylistActivity extends PageAdsAppCompactActivity {

    private ArrayList<YTVideoModel> playList = new ArrayList<>();
    private String playerName;
    private String channelId;
    private String playListIds;
    private YTVideoAdapter mAdapter;
    private String host;
    private SwipeRefreshLayout mySwipeRefreshLayout;
    private View llNoData;
    private int catId = 0;
    private boolean isShowBookmark = false;

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        if (playerName != null) {
            outState.putString(YTConstant.PLAYER_NAME, playerName);
        }
        if (channelId != null) {
            outState.putString(YTConstant.CHANNEL_ID, channelId);
        }
        if (playListIds != null) {
            outState.putString(YTConstant.PLAYLIST_ID, playListIds);
        }
        if (host != null) {
            outState.putString(YTConstant.API_HOST, host);
        }
        outState.putInt(YTConstant.CAT_ID, catId);
        outState.putBoolean(YTConstant.CAT_ID, isShowBookmark);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        YTUtility.setTranslucentColor(getWindow());
        super.onCreate(savedInstanceState);
        setContentView(R.layout.yt_activity_channel_playlist);

        initAds((RelativeLayout) findViewById(R.id.ll_ad ), this);
        setUpRecyclerView();
        if (savedInstanceState == null) {
            getBundle(getIntent());
        } else {
            playerName = savedInstanceState.getString(YTConstant.PLAYER_NAME);
            channelId = savedInstanceState.getString(YTConstant.CHANNEL_ID);
            playListIds = savedInstanceState.getString(YTConstant.PLAYLIST_ID);
            host = savedInstanceState.getString(YTConstant.API_HOST);
            catId = savedInstanceState.getInt(YTConstant.CAT_ID);
            isShowBookmark = savedInstanceState.getBoolean(YTConstant.IS_SHOW_BOOKMARK);
            loadData();
        }
        setupToolBar();
        updateMenuVisibility();
    }

    private void initAds(RelativeLayout relativeLayout, Activity activity) {
        if (AdsSDK.getInstance() != null ){
            AdsSDK.getInstance().setAdoptiveBannerAdsOnView(relativeLayout , activity);
        }
    }

    private void loadData() {
        try {
            if(!isShowBookmark) {
                if (mAdapter != null) {
                    mAdapter.setCatId(catId);
                }
                if (channelId != null) {
                    getPlaylistByChannelId(channelId);
                } else if (playListIds != null) {
                    getPlaylistByChannelId(playListIds);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void getBundle(Intent intent) {
        try {
//            host = intent.getStringExtra(YTConstant.API_HOST);
            playerName = intent.getStringExtra(YTConstant.PLAYER_NAME);
            channelId = intent.getStringExtra(YTConstant.CHANNEL_ID);
            playListIds = intent.getStringExtra(YTConstant.PLAYLIST_ID);
            catId = intent.getIntExtra(YTConstant.CAT_ID, 0);
            isShowBookmark = intent.getBooleanExtra(YTConstant.IS_SHOW_BOOKMARK, false);
            loadData();
        } catch (Exception e) {
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    private void setupToolBar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        if (!TextUtils.isEmpty(playerName)) {
            setSupportActionBar(toolbar);
            if (getSupportActionBar() != null) {
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                getSupportActionBar().setTitle(playerName);
            }
        } else {
            toolbar.setVisibility(View.GONE);
        }

    }


    private void setUpRecyclerView() {
        llNoData = findViewById(R.id.ll_no_data);
        RecyclerView recyclerView = findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        mAdapter = new YTVideoAdapter(this, YTType.PLAYLIST, playList, new OnItemClickListener<YTVideoModel>() {
            @Override
            public void onItemClick(YTVideoModel item, YTType ytType) {
                if(!ConfigUtil.isConnected(YTChannelPlaylistActivity.this)){
                    BaseUtil.showToast(YTChannelPlaylistActivity.this, BaseConstants.NO_INTERNET_CONNECTION);
                    return;
                }
                YTUtility.openInternalYoutubeByPlaylistId(YTChannelPlaylistActivity.this, host, item.getTitle(), item.getVideoId());
            }
        });
        try {
            mAdapter.setShowBookmark(!YTPlayer.getInstance().isMarkBookmarkDisable());
        } catch (Exception e) {
            e.printStackTrace();
        }
        recyclerView.setAdapter(mAdapter);

        mySwipeRefreshLayout = findViewById(R.id.swipe_refresh);
        mySwipeRefreshLayout.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        loadData();
                    }
                }
        );
    }

    private void hideProgressBar(int textVisibility) {
        if (mySwipeRefreshLayout != null) {
            mySwipeRefreshLayout.setRefreshing(false);
        }
        YTUtility.showNoData(llNoData, textVisibility);
    }

    private void populateRecyclerView(List<YTVideoModel> tempList) {
        playList.clear();
        playList.addAll(tempList);
        mAdapter.notifyDataSetChanged();
    }


    private void getPlaylistByChannelId(String channelOrPlayListIds) {
        ApiCall.request(host, YTNetwork.GET_PLAYLISTS_METADATA, ParamBuilder.getMultiplePlayListDetails(channelOrPlayListIds), new ApiCall.ApiResponse() {
            @Override
            public void onSuccess(String response) {
                hideProgressBar(View.GONE);
                YTVideoModel ytVideoModel = JsonParser.parsePlayList(response);
                if (TextUtils.isEmpty(ytVideoModel.getError())) {
                    populateRecyclerView(ytVideoModel.getList());
                    getBookmarkList(isShowBookmark);
                } else {
                    Toast.makeText(YTChannelPlaylistActivity.this, ytVideoModel.getError(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Exception e) {
                hideProgressBar(View.VISIBLE);
                Toast.makeText(YTChannelPlaylistActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onRetry(NetworkListener.Retry retryCallback) {
                if(playList == null || playList.size() <= 0) {
                    BaseUtil.showNoDataRetry(llNoData, retryCallback);
                }
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        getBookmarkList(isShowBookmark);
    }

    private void getBookmarkList(boolean isShowBookmark) {
        TaskRunner.getInstance().executeAsync(new Callable<Boolean>() {
            @Override
            public Boolean call() throws Exception {
                YTDbHelper dbHelper = YTPlayer.getInstance().getDBObject();
                if (dbHelper != null) {
                    dbHelper.callDBFunction(new Callable<Void>() {
                        @Override
                        public Void call() throws Exception {
                            if(!isShowBookmark){
                                HashMap<String, Integer> playListHashMap = dbHelper.getAllBookmarkPlayListHashMap(catId);
                                if(playListHashMap != null && playList.size() > 0) {
                                    for (YTVideoModel item : playList) {
                                        Integer isFav = playListHashMap.get(item.getVideoId());
                                        item.setIsFav(isFav != null ? isFav : YTDbHelper.IN_ACTIVE);
                                    }
                                }
                            }else {
                                List<YTVideoModel> mPlayLists = dbHelper.getAllBookmarkPlayList(catId);
                                playList.clear();
                                if (mPlayLists != null) {
                                    playList.addAll(mPlayLists);
                                }
                            }
                            return null;
                        }
                    });
                }
                return true;
            }
        }, new TaskRunner.Callback<Boolean>() {
            @Override
            public void onComplete(Boolean result) {
                if(isShowBookmark) {
                    if (playList.size() > 0) {
                        hideProgressBar(View.GONE);
                    } else {
                        hideProgressBar(View.VISIBLE);
                    }
                }
                if (mAdapter != null) {
                    mAdapter.setCatId(catId);
                    mAdapter.notifyDataSetChanged();
                }
            }
        });
    }

    private MenuItem menuBookmark;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.yt_menu_bookmark, menu);
        menuBookmark = menu.findItem(R.id.menu_item_bookmark);
        menuBookmark.setIcon(ContextCompat.getDrawable(this, R.drawable.ic_yt_menu_bookmark_all));
        updateMenuVisibility();
        return true;
    }

    private void updateMenuVisibility() {
        if(menuBookmark != null){
            try {
                if(YTPlayer.getInstance().isMarkBookmarkDisable()){
                    menuBookmark.setVisible(false);
                }else {
                    menuBookmark.setVisible(catId > 0 && !isShowBookmark);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        } else if (id == R.id.menu_item_bookmark) {
            YTUtility.openBookmarkChannels(this, catId, getString(R.string.channel_bookmarks));
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
