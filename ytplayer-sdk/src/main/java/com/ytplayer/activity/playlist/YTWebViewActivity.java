package com.ytplayer.activity.playlist;

import android.os.Bundle;
import android.webkit.WebView;

import androidx.annotation.Nullable;

import com.adssdk.AdsAppCompactActivity;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.views.YouTubePlayerView;
import com.ytplayer.R;
import com.ytplayer.util.SizeUtil;

public class YTWebViewActivity extends AdsAppCompactActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.yt_activity_webview);
        YouTubePlayerView youTubePlayerView = findViewById(R.id.youtube_player_view);
        getLifecycle().addObserver(youTubePlayerView);

    }
}
