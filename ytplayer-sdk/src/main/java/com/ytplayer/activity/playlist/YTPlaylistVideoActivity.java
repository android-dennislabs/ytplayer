package com.ytplayer.activity.playlist;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.adssdk.AdsSDK;
import com.adssdk.PageAdsAppCompactActivity;
import com.config.config.ConfigManager;
import com.google.gson.JsonSyntaxException;
import com.helper.callback.NetworkListener;
import com.helper.callback.Response;
import com.helper.task.TaskRunner;
import com.helper.util.BaseUtil;
import com.ytplayer.R;
import com.ytplayer.YTPlayer;
import com.ytplayer.YTUtility;
import com.ytplayer.adapter.OnItemClickListener;
import com.ytplayer.adapter.YTVideoAdapter;
import com.ytplayer.adapter.YTVideoModel;
import com.ytplayer.model.YTOtherProperty;
import com.ytplayer.model.YTPlaylistVideoModel;
import com.ytplayer.network.ApiCall;
import com.ytplayer.network.JsonParser;
import com.ytplayer.network.ParamBuilder;
import com.ytplayer.network.YTNetwork;
import com.ytplayer.task.YTGetWatchListTask;
import com.ytplayer.util.YTConstant;
import com.ytplayer.util.YTDbHelper;
import com.ytplayer.util.YTPlayList;
import com.ytplayer.util.YTType;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;

public class YTPlaylistVideoActivity extends PageAdsAppCompactActivity {

    private ArrayList<YTVideoModel> playList = new ArrayList<>();
    private String playerName;
    private String channelId;
    private YTVideoAdapter mAdapter;
    private String host;
    private SwipeRefreshLayout mySwipeRefreshLayout;
    private View llNoData;
    private Activity activity;
    private YTOtherProperty property;

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        if (playerName != null) {
            outState.putString(YTConstant.PLAYER_NAME, playerName);
        }
        if (channelId != null) {
            outState.putString(YTConstant.CHANNEL_ID, channelId);
        }

        if (host != null) {
            outState.putString(YTConstant.API_HOST, host);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        YTUtility.setTranslucentColor(getWindow());
        super.onCreate(savedInstanceState);
        setContentView(R.layout.yt_activity_channel_playlist);
        activity = this;

        initAds((RelativeLayout) findViewById(R.id.ll_ad), this);
        setUpRecyclerView();
        if (savedInstanceState == null) {
            getBundle(getIntent());
        } else {
            playerName = savedInstanceState.getString(YTConstant.PLAYER_NAME);
            channelId = savedInstanceState.getString(YTConstant.CHANNEL_ID);
            host = savedInstanceState.getString(YTConstant.API_HOST);
            getProperty(savedInstanceState.getString(YTConstant.DATA));
            property = (YTOtherProperty) savedInstanceState.getSerializable(YTConstant.DATA);
            loadData();
        }
        setupToolBar();
    }

    private void getProperty(String string) {
        try {
            property = ConfigManager.getGson().fromJson( string, YTOtherProperty.class );
        } catch (JsonSyntaxException e) {
            e.printStackTrace();
        }
    }

    private void initAds(RelativeLayout relativeLayout, Activity activity) {
        if (AdsSDK.getInstance() != null) {
            AdsSDK.getInstance().setAdoptiveBannerAdsOnView(relativeLayout, activity);
        }
    }

    private void loadData() {
        try {
            if (property != null) {
                getDataFromServer();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    protected void onStart() {
        super.onStart();
        updateList();
    }


    private void updateList() {
        if (playList.size() > 0) {
            new YTGetWatchListTask(playList).execute(channelId, new Response.Status<Boolean>() {
                @Override
                public void onSuccess(Boolean response) {
                    if (mAdapter != null) {
                        mAdapter.notifyDataSetChanged();
                    }
                }
            });
        }

    }

    private void getBundle(Intent intent) {
        try {
            playerName = intent.getStringExtra(YTConstant.PLAYER_NAME);
            channelId = intent.getStringExtra(YTConstant.CHANNEL_ID);
            getProperty(intent.getStringExtra(YTConstant.DATA));
            loadData();
        } catch (Exception e) {
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    private void setupToolBar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        if (!TextUtils.isEmpty(playerName)) {
            toolbar.setTitle(playerName);
            toolbar.setNavigationOnClickListener(
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            onBackPressed();
                        }
                    }
            );
        } else {
            toolbar.setVisibility(View.GONE);
        }

    }


    private void setUpRecyclerView() {
        llNoData = findViewById(R.id.ll_no_data);
        RecyclerView recyclerView = findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        mAdapter = new YTVideoAdapter(this, YTType.PLAYLIST, playList, new OnItemClickListener<YTVideoModel>() {
            @Override
            public void onItemClick(YTVideoModel item, YTType ytType) {
                if ( item.getYtType() == YTType.PLAYLIST ) {
                    YTUtility.openInternalYoutubeByPlaylistId(activity, host, item.getTitle(), item.getVideoId());
                } else if ( item.getYtType() == YTType.VIDEO ){
                    ArrayList<YTVideoModel> ytVideoModels = new ArrayList<>(1);
                    ytVideoModels.add(item);
                    YTUtility.openInternalYoutubePlaylistPlayer(activity, host, item.getTitle(), 0 , ytVideoModels);
                }
            }
        });
        recyclerView.setAdapter(mAdapter);

        mySwipeRefreshLayout = findViewById(R.id.swipe_refresh);
        mySwipeRefreshLayout.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        loadData();
                    }
                }
        );
    }

    private void hideProgressBar(int textVisibility) {
        if (mySwipeRefreshLayout != null) {
            mySwipeRefreshLayout.setRefreshing(false);
        }
        YTUtility.showNoData(llNoData, textVisibility);
    }

    private void populateRecyclerView(ArrayList<YTVideoModel> tempList) {
        playList.clear();
        playList.addAll(tempList);
        mAdapter.notifyDataSetChanged();
    }


    private void getDataFromServer() {
        ApiCall.request(host, YTNetwork.GET_PLAYLISTS_METADATA_V2, ParamBuilder.getMultiplePlayListVideo(property.getVideo_playlist_ids(), property.getVideo_ids()), new ApiCall.ApiResponse() {
            @Override
            public void onSuccess(String response) {
                hideProgressBar(View.GONE);
                if (property != null && property.getYtPlaylistVideoModels() != null && property.getYtPlaylistVideoModels().size() > 0) {
                    TaskRunner.getInstance().executeAsync(new Callable<YTVideoModel>() {
                        @Override
                        public YTVideoModel call() throws Exception {
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                                Collections.sort(property.getYtPlaylistVideoModels(), new Comparator<YTPlaylistVideoModel>() {
                                    @Override
                                    public int compare(YTPlaylistVideoModel o1, YTPlaylistVideoModel o2) {
                                        return Integer.compare(o1.getRank(), o2.getRank());
                                    }
                                });
                            }
                            YTVideoModel ytVideoModel = JsonParser.parsePlayListAndVideo(response, property.getYtPlaylistVideoModels());
                            YTGetWatchListTask.updateList(channelId, ytVideoModel.getList());
                            return ytVideoModel;
                        }
                    }, new TaskRunner.Callback<YTVideoModel>() {
                        @Override
                        public void onComplete(YTVideoModel ytVideoModel) {
                            if (TextUtils.isEmpty(ytVideoModel.getError())) {
                                populateRecyclerView(ytVideoModel.getList());
                            } else {
                                Toast.makeText(activity, ytVideoModel.getError(), Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                } else {
                    Toast.makeText(activity, "Error, Please try again later", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Exception e) {
                hideProgressBar(View.VISIBLE);
                Toast.makeText(activity, e.getMessage(), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onRetry(NetworkListener.Retry retryCallback) {
                if(playList == null || playList.size() <= 0) {
                    BaseUtil.showNoDataRetry(llNoData, retryCallback);
                }
            }
        });
    }
}

