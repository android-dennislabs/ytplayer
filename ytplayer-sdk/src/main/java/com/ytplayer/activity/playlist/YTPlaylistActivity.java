package com.ytplayer.activity.playlist;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.config.util.ConfigUtil;
import com.helper.callback.NetworkListener;
import com.helper.callback.Response;
import com.helper.task.TaskRunner;
import com.helper.util.BaseUtil;
import com.ytplayer.slidinguppanel.SlidingUpPanelLayout;
import com.ytplayer.R;
import com.ytplayer.YTPlayer;
import com.ytplayer.YTUtility;
import com.ytplayer.activity.YTBaseActivity;
import com.ytplayer.adapter.OnItemClickListener;
import com.ytplayer.adapter.YTVideoAdapter;
import com.ytplayer.adapter.YTVideoModel;
import com.ytplayer.network.ApiCall;
import com.ytplayer.network.JsonParser;
import com.ytplayer.network.ParamBuilder;
import com.ytplayer.network.YTNetwork;
import com.ytplayer.task.YTGetWatchListTask;
import com.ytplayer.task.YTInsertPlayListTask;
import com.ytplayer.util.YTConstant;
import com.ytplayer.util.YTDbHelper;
import com.ytplayer.util.YTPlayList;
import com.ytplayer.util.YTType;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

/**
 * @author Created by Abhijit on 2/6/2018.
 */
public class YTPlaylistActivity extends YTBaseActivity implements YTVideoAdapter.LoadMoreItems {

//    int maxCount = YTConfig.getMaxResultsCount();
//    int totalResultSize = YTConstant.TOTAL_RESULT_SIZE;

    private ArrayList<YTVideoModel> playList = new ArrayList<>();
    private String playerName;
    private YTVideoModel mYTVideoModel;
    private YTVideoAdapter mAdapter;
    private YTType itemType;
    private String host;
    private SwipeRefreshLayout mySwipeRefreshLayout;
    private View llNoData;
    private boolean canLoadMore = true;
    private View viewLoadMore;
    private boolean isNetWorkCall = false;
    private int positionAutoPlay = -1;
    private ImageView ivFavourite;
    private int mPosition = 0;
    private boolean isShowBookmark = false;

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        if (playList != null) {
            YTPlayList.setPlayList(playList);
        }
        if (playerName != null) {
            outState.putString(YTConstant.PLAYER_NAME, playerName);
        }
        if (playListId != null) {
            outState.putString(YTConstant.PLAYLIST_ID, playListId);
        }
        if (host != null) {
            outState.putString(YTConstant.API_HOST, host);
        }
        outState.putBoolean(YTConstant.CAT_ID, isShowBookmark);

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        YTUtility.setTranslucentColor(getWindow());
        super.onCreate(savedInstanceState);
        setContentView(R.layout.yt_activity_youtube_playlist);

        setUpRecyclerView();
        if (savedInstanceState == null) {
            getBundle(getIntent());
        } else {
            host = savedInstanceState.getString(YTConstant.API_HOST);
            playerName = savedInstanceState.getString(YTConstant.PLAYER_NAME);
            playListId = savedInstanceState.getString(YTConstant.PLAYLIST_ID);
            positionAutoPlay = savedInstanceState.getInt(YTConstant.POSITION, -1);
            isShowBookmark = savedInstanceState.getBoolean(YTConstant.IS_SHOW_BOOKMARK);
            ArrayList<YTVideoModel> tempList = YTPlayList.getPlayList();
            if (tempList != null) {
//                playList.addAll(tempList);ā
                itemType = YTType.LIST;
            } else {
                itemType = YTType.PLAYLIST_ID;
            }
            loadData(tempList);
        }
        setupToolBar();
        addSwipeListener();
    }

    private void addSwipeListener() {
        if (itemType == YTType.PLAYLIST_ID && !isAutoPlayValid()) {
            mySwipeRefreshLayout = findViewById(R.id.swipe_refresh);
            mySwipeRefreshLayout.setOnRefreshListener(
                    new SwipeRefreshLayout.OnRefreshListener() {
                        @Override
                        public void onRefresh() {
                            clearLastVideoCount();
                            loadData(null);
                        }
                    }
            );
        }
    }
    @Override
    protected void onResume() {
        super.onResume();
        if(isShowBookmark){
            getBookmarkVideosList();
        }
    }

    private void getBookmarkVideosList() {
        TaskRunner.getInstance().executeAsync(new Callable<Boolean>() {
            @Override
            public Boolean call() throws Exception {
                YTDbHelper dbHelper = YTPlayer.getInstance().getDBObject();
                if (dbHelper != null) {
                    dbHelper.callDBFunction(new Callable<Void>() {
                        @Override
                        public Void call() throws Exception {
                            List<YTVideoModel> mPlayLists = dbHelper.getAllBookmarkVideos();
                            playList.clear();
                            if (mPlayLists != null) {
                                playList.addAll(mPlayLists);
                            }
                            return null;
                        }
                    });
                }
                return true;
            }
        }, new TaskRunner.Callback<Boolean>() {
            @Override
            public void onComplete(Boolean result) {
                hideView(viewLoadMore);
                if(playList.size() > 0) {
                    if (mAdapter != null) {
                        mAdapter.stop();
                    }
                }else {
                    onProgressUpdateBar(View.VISIBLE);
                }
                if (mAdapter != null) {
                    mAdapter.setLoading(false);
                    mAdapter.notifyDataSetChanged();
                }
            }
        });
    }

    private void getBundle(Intent intent) {
        try {
            host = intent.getStringExtra(YTConstant.API_HOST);
            playerName = intent.getStringExtra(YTConstant.PLAYER_NAME);
            playListId = intent.getStringExtra(YTConstant.PLAYLIST_ID);
            positionAutoPlay = intent.getIntExtra(YTConstant.POSITION, -1);
            isShowBookmark = intent.getBooleanExtra(YTConstant.IS_SHOW_BOOKMARK, false);
            ArrayList<YTVideoModel> tempList = intent.getParcelableArrayListExtra(YTConstant.PLAYLIST);
            if (tempList != null) {
//                playList.addAll(tempList);
                itemType = YTType.LIST;
            } else {
                itemType = YTType.PLAYLIST_ID;
            }
            loadData(tempList);
            if (isAutoPlayValid() && tempList != null && tempList.size() > positionAutoPlay) {
                playVideo(tempList.get(positionAutoPlay));
            }

        } catch (Exception e) {
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    private void loadData(ArrayList<YTVideoModel> mList) {
        if(!isShowBookmark) {
            if (itemType == YTType.LIST) {
                if (mList != null && mList.size() > 0) {
                    populateRecyclerView(mList);
                }
            } else {
                if (playListId != null) {
                    getPlaylistByChannelId();
                }
            }
        }else {
            if (mySwipeRefreshLayout != null) {
                mySwipeRefreshLayout.setRefreshing(false);
            }
        }
    }

    private boolean isAutoPlayValid() {
        return positionAutoPlay >= 0;
    }

    private void setUpRecyclerView() {
        llNoData = findViewById(R.id.ll_no_data);
        viewLoadMore = findViewById(R.id.ll_load_more);
        ivFavourite = findViewById(R.id.iv_favourite);
        RecyclerView recyclerView = findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        mAdapter = new YTVideoAdapter(this, YTType.VIDEO, playList, new OnItemClickListener<YTVideoModel>() {
            @Override
            public void onItemClick(YTVideoModel item, YTType ytType) {
                item.setHost(host);
                playVideo(item);
                YTUtility.openYoutubePlaySingleVideoActivity( YTPlaylistActivity.this, item);
//                if (mVideoModel != null) {
//                    syncMenuIcon(mVideoModel.getVideoId());
//                }
            }

            @Override
            public void onItemUpdate(int position) {
                mPosition = position;
            }
        }).setMinimumItemCount(YTConstant.MIN_RESULTS_COUNT)
                .setOnLoadMoreListener(this);

        recyclerView.setAdapter(mAdapter);

        try {
            ivFavourite.setVisibility(YTPlayer.getInstance().isMarkFavouriteDisable() ? View.GONE : View.VISIBLE);
        } catch (Exception e) {
            e.printStackTrace();
        }
        ivFavourite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                markVideoFavourite();
            }
        });
    }

    private void markVideoFavourite() {
        if (mVideoModel != null && mPosition >= 0 && playList.size() > 0 && playList.size() > mPosition) {
            mVideoModel.setIsFav(mVideoModel.getIsFav() == YTDbHelper.ACTIVE ? YTDbHelper.IN_ACTIVE : YTDbHelper.ACTIVE);
            playList.get(mPosition).setIsFav(mVideoModel.getIsFav());
            //update database in background thread here
            mVideoModel.setYtType(YTType.VIDEO);
            new YTInsertPlayListTask(mVideoModel).execute();
            updateVideoFavouriteIcon();
        }
    }

    private void updateVideoFavouriteIcon() {
        if (ivFavourite != null && mVideoModel != null) {
            ivFavourite.setImageResource(mVideoModel.getIsFav() == YTDbHelper.ACTIVE ? R.drawable.ic_yt_menu_favourite_filled : R.drawable.ic_yt_menu_favourite);
        }
    }


    /**
     * mYTVideoModel : response getting from server
     * playList : videos list
     * totalResultSize : total video list count available on Playlist getting from server
     * maxCount : list pagination maximum count
     */
    @Override
    public void onLoadMore() {
        if (mYTVideoModel != null) {
            if (BaseUtil.isConnected(this)) {
                if (!isNetWorkCall && viewLoadMore != null && viewLoadMore.getVisibility() == View.GONE && canLoadMore) {
                    showView(viewLoadMore);
                    loadData(null);
                } else {
                    mAdapter.stop();
                }
            } else {
                hideView(viewLoadMore);
            }
        }
    }

    private void hideView(View view) {
        if (view != null) {
            view.setVisibility(View.GONE);
        }
    }

    private void showView(View view) {
        if (view != null) {
            view.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onProgressUpdateBar(int visibility) {
        if (mySwipeRefreshLayout != null) {
            mySwipeRefreshLayout.setRefreshing(false);
        }
        BaseUtil.showNoData(llNoData, visibility);
    }


    private void populateRecyclerView(ArrayList<YTVideoModel> tempList) {
        mAdapter.finish();
        if (getLastVideoCount() == 0) {
            playList.clear();
        }
        if (tempList != null && tempList.size() > 0) {
            playList.addAll(tempList);
            mAdapter.notifyDataSetChanged();
        }
        hideView(viewLoadMore);
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (playList.size() > 0) {
            new YTGetWatchListTask(playList).execute(playListId, new Response.Status<Boolean>() {
                @Override
                public void onSuccess(Boolean response) {
                    if (mAdapter != null) {
                        mAdapter.notifyDataSetChanged();
                    }
                }
            });
        }
    }

    private int getLastVideoCount() {
        return lastVideoCount;
    }

    int lastVideoCount = 0;

    private void clearLastVideoCount() {
        lastVideoCount = 0;
    }

    private void setLastVideoCount() {
        if (playList == null || playList.size() < 1) {
            lastVideoCount = 0;
        } else {
            lastVideoCount = playList.get(playList.size() - 1).getVideoCount();
        }
    }


    private void setupToolBar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        if (!TextUtils.isEmpty(playerName)) {
            toolbar.setTitle(playerName);
            toolbar.setNavigationOnClickListener(
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            onBackPressed();
                        }
                    }
            );
        } else {
            toolbar.setVisibility(View.GONE);
        }
    }

    private void syncMenuIcon(String videoId) {
        TaskRunner.getInstance().executeAsync(new Callable<Boolean>() {
            @Override
            public Boolean call() throws Exception {
                YTDbHelper dbHelper = YTPlayer.getInstance().getDBObject();
                if (dbHelper != null) {
                    dbHelper.callDBFunction(new Callable<Void>() {
                        @Override
                        public Void call() throws Exception {
                            int isFav = dbHelper.getPlayListFavouriteById(videoId);
                            mVideoModel.setIsFav(isFav);
                            return null;
                        }
                    });
                }
                return true;
            }
        }, new TaskRunner.Callback<Boolean>() {
            @Override
            public void onComplete(Boolean result) {
                updateVideoFavouriteIcon();
            }
        });
    }


    @Override
    public void initYTPlayer() {
        dragView = findViewById(R.id.dragView);
        slidingLayout = findViewById(R.id.sliding_layout);
//        playerView = findViewById(R.id.youtubePlayerView);
        scrollView = findViewById(R.id.scroll_view_video_detail);
    }

//    @Override
//    public void onInitializationSuccess() {
//
//    }

    @Override
    public void onPanelStateChanged(SlidingUpPanelLayout.PanelState newState) {

    }

    @Override
    public void onUpdateList(YTVideoModel item) {
        if (playList != null && playList.size() > 0) {
            for (YTVideoModel video : playList) {
                if (video.getVideoId().equalsIgnoreCase(item.getVideoId())) {
                    YTGetWatchListTask.updateModel(video, item);
                    break;
                }
            }
            if (mAdapter != null) {
                mAdapter.notifyDataSetChanged();
            }
        }
    }

    private void getPlaylistByChannelId() {
        if (!isNetWorkCall) {
            isNetWorkCall = true;
            ApiCall.request(host, YTNetwork.GET_PLAYLIST_ITEMS_PAGINATION, ParamBuilder.getPlayListItems(playListId, getLastVideoCount()), new ApiCall.ApiResponse() {
                @Override
                public void onSuccess(String response) {
                    TaskRunner.getInstance().executeAsync(new Callable<YTVideoModel>() {
                        @Override
                        public YTVideoModel call() throws Exception {
                            YTVideoModel ytVideoModel = JsonParser.parsePlayListItems(response);
                            YTGetWatchListTask.updateList(playListId, ytVideoModel.getList());
                            return ytVideoModel;
                        }
                    }, new TaskRunner.Callback<YTVideoModel>() {
                        @Override
                        public void onComplete(YTVideoModel ytVideoModel) {
                            onProgressUpdateBar(View.GONE);
                            canLoadMore = ytVideoModel.isLoadMore();
                            if (TextUtils.isEmpty(ytVideoModel.getError())) {
                                mYTVideoModel = ytVideoModel;
                                if (ytVideoModel.getList().size() < 1) {
                                    canLoadMore = false;
                                }
                                populateRecyclerView(ytVideoModel.getList());
                                setLastVideoCount();
                            } else {
                                if(ConfigUtil.isConnected(YTPlaylistActivity.this)) {
                                    Toast.makeText(YTPlaylistActivity.this, ytVideoModel.getError(), Toast.LENGTH_SHORT).show();
                                }else {
                                    if(playList == null || playList.size() == 0){
                                        BaseUtil.showNoData(llNoData, View.VISIBLE);
                                    }
                                }
                            }
                            isNetWorkCall = false;
                        }
                    });
                }

                @Override
                public void onFailure(Exception e) {
                    if (playList == null || playList.size() < 1) {
                        onProgressUpdateBar(View.VISIBLE);
                        if(ConfigUtil.isConnected(YTPlaylistActivity.this)) {
                            Toast.makeText(YTPlaylistActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        onProgressUpdateBar(View.GONE);
                    }
                    canLoadMore = false;
                    populateRecyclerView(null);
                    isNetWorkCall = false;
                    hideView(viewLoadMore);
                    if(playList == null || playList.size() == 0){
                        BaseUtil.showNoData(llNoData, View.VISIBLE);
                    }
                }

                @Override
                public void onRetry(NetworkListener.Retry retryCallback) {
                    if(playList == null || playList.size() <= 0) {
                        BaseUtil.showNoDataRetry(llNoData, retryCallback);
                    }
                }
            });
        }
    }
}