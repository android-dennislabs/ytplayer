package com.ytplayer.activity;

import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;

import com.adssdk.PageAdsAppCompactActivity;
import com.config.util.ConfigUtil;

import com.helper.util.BaseConstants;
import com.helper.util.BasePrefUtil;
import com.helper.util.BaseUtil;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.PlayerConstants;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.YouTubePlayer;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.listeners.AbstractYouTubePlayerListener;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.listeners.FullscreenListener;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.options.IFramePlayerOptions;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.views.YouTubePlayerView;
import com.ytplayer.R;
import com.ytplayer.YTPlayer;
import com.ytplayer.YTUtility;
import com.ytplayer.adapter.YTVideoModel;
import com.ytplayer.adapter.YTVideoStatistics;
import com.ytplayer.slidinguppanel.SlidingUpPanelLayout;
import com.ytplayer.task.YTInsertPlayListTask;
import com.ytplayer.task.YTInsertWatchListTask;
import com.ytplayer.util.AnimUtil;
import com.ytplayer.util.Logger;
import com.ytplayer.util.SizeUtil;
import com.ytplayer.util.YTConfig;
import com.ytplayer.util.YTDbHelper;
import com.ytplayer.util.YTType;
import com.ytplayer.views.YTPlayerTextView;

import java.text.DecimalFormat;

import kotlin.Unit;
import kotlin.jvm.functions.Function0;


/**
 * @author Created by Abhijit on 2/6/2018.
 */
public abstract class YTBaseActivity extends PageAdsAppCompactActivity implements SlidingUpPanelLayout.PanelSlideListener {

//    protected YouTubePlayer youTubePlayer;
    protected int playerWidth;
    protected int playerHeight;
    protected LinearLayout dragView;
    protected SlidingUpPanelLayout slidingLayout;
    protected SlidingUpPanelLayout.PanelState currentState;
    private boolean wasRestored;
    protected String videoId, videoTitle, videoDescription, videoPublishedAt;
    private TextView tvPublishedDate, tvVideoTitle, tvCollapsedVideoTitle, tvCollapsedVideoChannel, tvVideoViews, tvVideoLikes, tvVideoDisLikes, tvVideoDescription;
    private ImageView ivFavourite;
    private RelativeLayout frameLayout;
    private boolean isFullScreen;
    private int mSlideOffset;
    private LinearLayout llVideoDetail;
    private ProgressBar pbVideoProgress;
    protected ScrollView scrollView;
    protected YTVideoModel mVideoModel;
    protected boolean isSinglePlayVideo = false;
    private String chanelTitle;
    protected String playListId;

    int currentTimeMillis = 0;

    private int getCurrentTimeMillis(){
        return currentTimeMillis;
    }

    public abstract void initYTPlayer();

//    public abstract void onInitializationSuccess();

    public abstract void onPanelStateChanged(SlidingUpPanelLayout.PanelState newState);

    public abstract void onUpdateList(YTVideoModel item);

    @Override
    protected void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        try {
            if (videoId != null) {
                bundle.putString("videoId", videoId);
            }
            if (videoTitle != null) {
                bundle.putString("videoTitle", videoTitle);
            }
            if (videoDescription != null) {
                bundle.putString("videoDescription", videoDescription);
            }
            if (videoPublishedAt != null) {
                bundle.putString("videoPublishedAt", videoPublishedAt);
            }
                bundle.putInt("videoTime", getCurrentTimeMillis());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        if (bundle != null) {
            videoId = bundle.getString("videoId");
            currentTimeMillis = bundle.getInt("videoTime");
            videoTitle = bundle.getString("videoTitle");
            videoDescription = bundle.getString("videoDescription");
            videoPublishedAt = bundle.getString("videoPublishedAt");
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (!isSinglePlayVideo) {
            init();
        }
    }

    private boolean hasPrevious = false;


    public void playVideo(YTVideoModel model) {
        if(!ConfigUtil.isConnected(this)){
            BaseUtil.showToast(this, BaseConstants.NO_INTERNET_CONNECTION);
            return;
        }
        if(youTubePlayer != null || hasPrevious){
            maintainHistory(mVideoModel.getClone());
            hasPrevious = true;
        }
        this.mVideoModel = model;
        this.videoTitle = model.getTitle();
        this.videoDescription = model.getDescription();
        this.videoPublishedAt = model.getPublishedAt();
        this.chanelTitle = model.getChannelTitle();
        this.videoId = model.getVideoId();
        this.currentTimeMillis = getLastVideoTimePref();

        mVideoModel.setIsFav( getVideoPrefFav() ? YTDbHelper.ACTIVE : YTDbHelper.IN_ACTIVE );
        updateVideoFavouriteIcon();

        playVideo(videoId);
    }


    public void playVideo(String mVideoId) {
        this.videoId = mVideoId;
        updateVideoDetails(videoId);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (youTubePlayer != null) {
                    try {
                        if (!TextUtils.isEmpty(videoId)) {
                            dragView.setVisibility(View.VISIBLE);
                            youTubePlayer.cueVideo(videoId, getCurrentTimeSec());

                            if (slidingLayout != null) {
                                currentState = SlidingUpPanelLayout.PanelState.EXPANDED;
                                slidingLayout.setPanelState(currentState);
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        initializeYoutubePlayer();
                    }
                } else {
                    initializeYoutubePlayer();
                }
            }
        }, 500);

    }

    private float getCurrentTimeSec() {
        return (float) (getCurrentTimeMillis() > 0 ? getCurrentTimeMillis()/1000 : 0 );
    }

    protected void init() {
        if (youTubePlayerView == null) {
            initYTPlayer();
            if (!isSinglePlayVideo && dragView == null && slidingLayout == null && youTubePlayerView == null) {
                Toast.makeText(this, "Initialization failed.", Toast.LENGTH_SHORT).show();
                return;
            }
            frameLayout = findViewById(R.id.frameLayout);
            llVideoDetail = findViewById(R.id.llVideoDetail);
            pbVideoProgress = findViewById(R.id.pb_video_progress);
            tvPublishedDate = findViewById(R.id.tv_video_published);
            tvCollapsedVideoTitle = findViewById(R.id.tv_collapsed_video_title);
            tvCollapsedVideoChannel = findViewById(R.id.tv_collapsed_video_channel);
            tvVideoTitle = findViewById(R.id.tv_video_title);
            tvVideoViews = findViewById(R.id.tv_video_views);
            tvVideoLikes = findViewById(R.id.tv_video_likes);
            tvVideoDisLikes = findViewById(R.id.tv_video_dis_likes);
            tvVideoDescription = findViewById(R.id.tv_video_description);
            ivFavourite = findViewById(R.id.iv_favourite);
            if(findViewById(R.id.btnClose) != null) {
                (findViewById(R.id.btnClose)).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        closeYoutubePlayer();
                    }
                });
            }

            initializeYoutubePlayer();

            YTPlayerTextView.applyFont(tvPublishedDate, tvVideoTitle, tvCollapsedVideoTitle, tvCollapsedVideoChannel, tvVideoViews, tvVideoLikes, tvVideoDisLikes, tvVideoDescription);

            try {
                ivFavourite.setVisibility(YTPlayer.getInstance().isMarkFavouriteDisable() ? View.GONE : View.VISIBLE);
            } catch (Exception e) {
                e.printStackTrace();
            }
            ivFavourite.setOnClickListener(v -> markVideoFavourite());
        }
    }

    private void closeYoutubePlayer() {
        try {
            if (slidingLayout != null) {
                slidingLayout.setPanelState(SlidingUpPanelLayout.PanelState.HIDDEN);
                slidingLayout.setShadowHeight(0);
            }
            if (youTubePlayer != null) {
                youTubePlayer.pause();
            }
            if (dragView != null) {
                dragView.setVisibility(View.GONE);
            }
            resetPadding();
            maintainHistory(mVideoModel);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void maintainHistory(YTVideoModel mVideoModel) {
        try {
            if (youTubePlayer != null) {
                mVideoModel.setVideoTime(getCurrentTimeMillis());
                mVideoModel.setChannelId(playListId);
            }
            YTPlayer.getInstance().insertHistory(mVideoModel);
            new YTInsertWatchListTask(mVideoModel).execute();
            onUpdateList(mVideoModel);
        } catch (Exception e) {
            e.printStackTrace();
        }

        BasePrefUtil.setInt( this , YT_VIDEO_TIME + videoId , getCurrentTimeMillis() );
    }

    private int getLastVideoTimePref(){
        return BasePrefUtil.getInt( this , YT_VIDEO_TIME + videoId , 0 );
    }


    private boolean getVideoPrefFav(){
        return BasePrefUtil.getBoolean( this , YT_VIDEO_FAV + videoId , false );
    }

    private void setVideoPrefFav(){
        BasePrefUtil.setBoolean( this , YT_VIDEO_FAV + videoId , ( mVideoModel.getIsFav() == YTDbHelper.ACTIVE ) );
    }


    public static final String YT_VIDEO_TIME = "yt_video_time_";
    public static final String YT_VIDEO_FAV = "yt_video_fav_";

    private void markVideoFavourite() {
        if (mVideoModel != null ) {
            mVideoModel.setIsFav(mVideoModel.getIsFav() == YTDbHelper.ACTIVE ? YTDbHelper.IN_ACTIVE : YTDbHelper.ACTIVE);
            //update database in background thread here
            mVideoModel.setYtType(YTType.VIDEO);
            new YTInsertPlayListTask(mVideoModel).execute();
            setVideoPrefFav();
            updateVideoFavouriteIcon();
        }
    }

    private void updateVideoFavouriteIcon() {
        if (ivFavourite != null && mVideoModel != null) {
            ivFavourite.setImageResource(mVideoModel.getIsFav() == YTDbHelper.ACTIVE ? R.drawable.ic_yt_menu_favourite_filled : R.drawable.ic_yt_menu_favourite);
        }
    }


    private YouTubePlayer youTubePlayer;
    YouTubePlayerView youTubePlayerView;
    FrameLayout fullscreenViewContainer;
    private void initializeYoutubePlayer() {
        if ( !isSinglePlayVideo )
            return;

        youTubePlayerView = findViewById(R.id.youtube_player_view);
        fullscreenViewContainer = findViewById(R.id.full_screen_view_container);
        IFramePlayerOptions iFramePlayerOptions = new IFramePlayerOptions.Builder()
                .controls(1)
                .fullscreen(0) // enable full screen button
                .build();
        youTubePlayerView.setEnableAutomaticInitialization(false);
        youTubePlayerView.addFullscreenListener(new FullscreenListener() {
            @Override
            public void onEnterFullscreen(@NonNull View fullscreenView, @NonNull Function0<Unit> function0) {
                isFullScreen = true;
                youTubePlayerView.setVisibility(View.GONE);
                fullscreenViewContainer.setVisibility(View.VISIBLE);
                fullscreenViewContainer.addView(fullscreenView);
            }

            @Override
            public void onExitFullscreen() {
               isFullScreen = false;
                youTubePlayerView.setVisibility(View.VISIBLE);
                fullscreenViewContainer.setVisibility(View.GONE);
                fullscreenViewContainer.removeAllViews();
            }
        });

        youTubePlayerView.initialize(new AbstractYouTubePlayerListener() {
            @Override
            public void onCurrentSecond(@NonNull com.pierfrancescosoffritti.androidyoutubeplayer.core.player.YouTubePlayer youTubePlayer, float second) {
                super.onCurrentSecond(youTubePlayer, second);
                currentTimeMillis =  (int) (second > 0 ? second*1000 : 0);
            }

            @Override
            public void onStateChange(@NonNull YouTubePlayer youTubePlayer, @NonNull PlayerConstants.PlayerState state) {
                super.onStateChange(youTubePlayer, state);
                if ( state == PlayerConstants.PlayerState.PAUSED
                        || state == PlayerConstants.PlayerState.BUFFERING
                        || state == PlayerConstants.PlayerState.ENDED
                ){
                    mVideoModel.setVideoTime(getCurrentTimeMillis());
                    maintainHistory(mVideoModel);
                }
            }

            @Override
            public void onReady(@NonNull com.pierfrancescosoffritti.androidyoutubeplayer.core.player.YouTubePlayer youTubePlayer) {
                super.onReady(youTubePlayer);
                YTBaseActivity.this.youTubePlayer = youTubePlayer;
                youTubePlayer.loadVideo(videoId, getCurrentTimeSec());
            }
        }, iFramePlayerOptions);

    }

    public void openYoutubeApp(){
        if ( SizeUtil.isYoutubeAppAvailable(this) ) {
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("vnd.youtube:"+videoId));
            intent.putExtra("VIDEO_ID", videoId);
            startActivity(intent);
        } else {
            BaseUtil.showToast( this , "Youtube App not found." );
        }
    }

    private void initializeYoutubePlayerOld() {
       /* String apiKey = YTConfig.getApiKey();
        if (TextUtils.isEmpty(apiKey)) {
            apiKey = getString(R.string.google_api_key);
            if (TextUtils.isEmpty(apiKey)) {
                if (!TextUtils.isEmpty(videoId)) {
                    YTUtility.openYoutubeApp(this, videoId);
                }
                finish();
            }
        }
        playerView.initialize(apiKey, this);
        dragView.setVisibility(View.GONE);
        if (slidingLayout != null) {
            slidingLayout.addPanelSlideListener(this);
        }*/
    }

   /* @Override
    public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean wasRestored) {
        this.youTubePlayer = youTubePlayer;
        this.wasRestored = wasRestored;
        onInitializationSuccess();
        Logger.log("onInitializationSuccess");

        youTubePlayer.setPlaybackEventListener(playbackEventListener);
        youTubePlayer.setPlayerStateChangeListener(playerStateChangeListener);

        youTubePlayer.setFullscreenControlFlags(YouTubePlayer.FULLSCREEN_FLAG_CONTROL_ORIENTATION);

        if (!TextUtils.isEmpty(videoId)) {
            playVideo(videoId);
        }
        getPlayerSize();

        youTubePlayer.setOnFullscreenListener(new YouTubePlayer.OnFullscreenListener() {
            @Override
            public void onFullscreen(boolean b) {
                isFullScreen = b;
            }
        });
    }*/

    /*private void getPlayerSize() {
        try {
//            int[] size = SizeUtil.getScreenWidthAndHeight(this);
            playerWidth = playerView.getMeasuredWidth();
            playerHeight = playerView.getMeasuredHeight();
        } catch (Exception e) {
            playerWidth = 0;
            playerHeight = 0;
        }
    }


    //Toast pop up messaging to show errors.
    @Override
    public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {
        final int REQUEST_CODE = 1;

        if (youTubeInitializationResult.isUserRecoverableError()) {
            youTubeInitializationResult.getErrorDialog(this, REQUEST_CODE).show();

        } else {
            String errorMessage = String.format("There was an error initializing the YoutubePlayer " + youTubeInitializationResult.toString());
            Toast.makeText(this, errorMessage, Toast.LENGTH_LONG).show();
        }

    }

    private YouTubePlayer.PlaybackEventListener playbackEventListener = new YouTubePlayer.PlaybackEventListener() {
        @Override
        public void onPlaying() {
            Logger.log("onPlaying");
        }

        @Override
        public void onPaused() {
            Logger.log("onPaused");
            videoTime = youTubePlayer.getCurrentTimeMillis();
        }

        @Override
        public void onStopped() {
            Logger.log("onStopped");
        }

        @Override
        public void onBuffering(boolean b) {
            Logger.log("onBuffering");
        }

        @Override
        public void onSeekTo(int i) {
            Logger.log("onSeekTo");
        }
    };

    private YouTubePlayer.PlayerStateChangeListener playerStateChangeListener = new YouTubePlayer.PlayerStateChangeListener() {
        @Override
        public void onLoading() {
            Logger.log("onLoading");
        }

        @Override
        public void onLoaded(String s) {
            Logger.log("onLoaded");
            youTubePlayer.play();
        }

        @Override
        public void onAdStarted() {
            Logger.log("onAdStarted");
        }

        @Override
        public void onVideoStarted() {
            Logger.log("onVideoStarted");
        }

        @Override
        public void onVideoEnded() {
            Logger.log("onVideoEnded");
        }

        @Override
        public void onError(YouTubePlayer.ErrorReason errorReason) {
            Logger.log("onError");
        }
    };
*/
    @Override
    public void onPanelSlide(View panel, float slideOffset) {
        DecimalFormat decimalFormat = new DecimalFormat("#.#");
        float twoDigitsF = Float.valueOf(decimalFormat.format(slideOffset));
        updateYoutubeLayout((int) (twoDigitsF * 100));
    }

    @Override
    public void onPanelStateChanged(View panel, SlidingUpPanelLayout.PanelState previousState, SlidingUpPanelLayout.PanelState newState) {
        try {
            currentState = newState;
            onPanelStateChanged(newState);
            if (newState == SlidingUpPanelLayout.PanelState.EXPANDED) {
                Log.d("@onPanelStateChanged", "EXPANDED");
//                updateLayoutParams(YouTubePlayerView.LayoutParams.MATCH_PARENT, YouTubePlayerView.LayoutParams.WRAP_CONTENT);
//                youTubePlayer.setPlayerStyle(YouTubePlayer.PlayerStyle.DEFAULT);
                youTubePlayer.play();
                currentState = SlidingUpPanelLayout.PanelState.EXPANDED;
                dragView.setBackgroundColor(Color.TRANSPARENT);
                scrollView.setBackgroundColor(ContextCompat.getColor(this, R.color.themeWindowBackground));
                resetPadding();
            } else if (newState == SlidingUpPanelLayout.PanelState.COLLAPSED) {
                Log.d("@onPanelStateChanged", "COLLAPSED");
                try {
//                    updateLayoutParams(SizeUtil.dpToPx(125), SizeUtil.dpToPx(85));
//                    updateLayoutParams(SizeUtil.dpToPx(200), SizeUtil.dpToPx(110));
                } catch (Exception e) {
                    e.printStackTrace();
                }
//                youTubePlayer.setPlayerStyle(YouTubePlayer.PlayerStyle.MINIMAL);
//                if(youTubePlayer.isPlaying()) {
//                    youTubePlayer.play();
//                }
                currentState = SlidingUpPanelLayout.PanelState.COLLAPSED;
                if (slidingLayout != null) {
                    slidingLayout.setShadowHeight(10);
                }
    //            dragView.setBackgroundColor(Color.WHITE);
                dragView.setBackgroundColor(ContextCompat.getColor(this, R.color.themeWindowBackground));
                frameLayout.setPadding(0, 0, 0, 350);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void updateYoutubeLayout(int slideOffset) {
        if (slidingLayout != null) {
            this.mSlideOffset = slideOffset;
            Log.d("@SlidingUpPanelLayout", "" + slideOffset);
            SlidingUpPanelLayout.PanelState panelState = slidingLayout.getPanelState();
            if (panelState == SlidingUpPanelLayout.PanelState.COLLAPSED) {
                Log.d("@SlidingUpPanelLayout", "COLLAPSED-" + slideOffset);

            } else if (panelState == SlidingUpPanelLayout.PanelState.DRAGGING) {
                Log.d("@SlidingUpPanelLayout", "DRAGGING-" + slideOffset);
                if (slideOffset > 0 && slideOffset < 100) {
                    updateViewsColor(slideOffset, scrollView, tvVideoViews, tvPublishedDate, tvVideoDescription, tvVideoDisLikes, tvVideoLikes, tvVideoTitle);
                }
                dragView.setBackgroundColor(Color.TRANSPARENT);

            } else if (panelState == SlidingUpPanelLayout.PanelState.EXPANDED) {
                Log.d("@SlidingUpPanelLayout", "EXPANDED-" + slideOffset);
            } else if (panelState == SlidingUpPanelLayout.PanelState.ANCHORED) {
                Log.d("@SlidingUpPanelLayout", "ANCHORED-" + slideOffset);
            }
        }
//        try {
//            if (slideOffset == 0) {
//                updateLayoutParams(SizeUtil.dpToPx(125), SizeUtil.dpToPx(85));
//                youTubePlayer.pause();
//                currentState = SlidingUpPanelLayout.PanelState.COLLAPSED;
//                slidingLayout.setShadowHeight(10);
//                dragView.setBackgroundColor(Color.WHITE);
//            } else if (slideOffset == 100) {
//                updateLayoutParams(YouTubePlayerView.LayoutParams.MATCH_PARENT, YouTubePlayerView.LayoutParams.WRAP_CONTENT);
//                youTubePlayer.play();
//                currentState = SlidingUpPanelLayout.PanelState.EXPANDED;
//                dragView.setBackgroundColor(Color.TRANSPARENT);
//            }
////            else {
////                updateLayoutParams(slideOffset);
////            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
    }

    private void updateViewsColor(int slideOffset, ScrollView scrollView, TextView... views) {
        try {
            String backgroundColor = "#" + slideOffset + Integer.toHexString(ContextCompat.getColor(this, R.color.themeWindowBackground) & 0x00ffffff);

            String textColor = "#" + slideOffset + Integer.toHexString(ContextCompat.getColor(this, R.color.themeTextColor) & 0x00ffffff);
            scrollView.setBackgroundColor(Color.parseColor(backgroundColor));
            for (TextView textView : views) {
                textView.setTextColor(Color.parseColor(textColor));
            }
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        if (isFullScreen) {
//            youTubePlayer.setFullscreen(false);
            youTubePlayer.toggleFullscreen();
            if (slidingLayout != null) {
                slidingLayout.setPanelState(SlidingUpPanelLayout.PanelState.EXPANDED);
            }
        } else {
            if (!isSinglePlayVideo) {
                if (slidingLayout.getPanelState() == SlidingUpPanelLayout.PanelState.COLLAPSED) {
                    closeYoutubePlayer();
                } else if (slidingLayout.getPanelState() == SlidingUpPanelLayout.PanelState.HIDDEN) {
                    super.onBackPressed();
                } else {
                    slidingLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
                }
            } else {
                mVideoModel.setVideoTime(getCurrentTimeMillis());
                maintainHistory(mVideoModel);
                super.onBackPressed();
            }
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    private void resetPadding() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                frameLayout.setPadding(0, 0, 0, 0);
            }
        }, 100);
    }

//    private void updateLayoutParams(int slideOffset) throws Exception {
//        int width = 0, height = 0;
//        int currentPlayerWidth = playerView.getMeasuredWidth();
//        int currentPlayerHeight = playerView.getMeasuredHeight();
//        if (currentState == SlidingUpPanelLayout.PanelState.COLLAPSED) {
//            width = playerWidth + getLayoutSize(playerWidth, slideOffset);
//            height = playerHeight + getLayoutSize(playerHeight, slideOffset);
//        } else if (currentState == SlidingUpPanelLayout.PanelState.EXPANDED) {
//            width = playerWidth - getLayoutSize(playerWidth, slideOffset);
//            height = playerHeight - getLayoutSize(playerHeight, slideOffset);
//        }
//        updateLayoutParams(SizeUtil.pxToDp(width), SizeUtil.pxToDp(height));
//    }

    private int getLayoutSize(int screenSize, int slideOffset) {
        return (screenSize * slideOffset) / 100;
    }

//    private void updateLayoutParams(int width, int height) {
//        int fromWidth = playerView.getMeasuredWidth();
//        int fromHeight = playerView.getMeasuredHeight();
//        if (width == YouTubePlayerView.LayoutParams.MATCH_PARENT) {
//            AnimUtil.resizeView(playerView, fromWidth, fromHeight, playerWidth, playerHeight);
//        } else {
//            AnimUtil.resizeView(playerView, fromWidth, fromHeight, width, height);
//        }
//        YouTubePlayerView.LayoutParams params = playerView.getLayoutParams();
//        params.width = width;
//        params.height = height;
//        playerView.setLayoutParams(params);
//    }


    private void updateVideoDetails(String videoId) {
        updateVideoDetailInUI();
        if (mVideoModel != null && mVideoModel.getStatistics() != null) {
            updateVideoUi(mVideoModel.getStatistics());
        } else {
            getVideoDetailById(videoId);
        }

    }

    private void updateVideoDetailInUI() {
        if (videoTitle != null) {
            tvVideoTitle.setText(videoTitle);
            if (tvCollapsedVideoTitle != null) {
                tvCollapsedVideoTitle.setText(videoTitle);
            }
        }
        if(tvCollapsedVideoTitle !=null) {
            if (chanelTitle != null) {
                tvCollapsedVideoChannel.setText(chanelTitle);
                tvCollapsedVideoChannel.setVisibility(View.VISIBLE);
            } else {
                tvCollapsedVideoChannel.setVisibility(View.GONE);
            }
        }
        if (videoDescription != null)
            tvVideoDescription.setText(videoDescription);
        if (videoPublishedAt != null)
            tvPublishedDate.setText(SizeUtil.formatDate(videoPublishedAt));
    }


    private void getVideoDetailById(String videoId) {
//        new VideoDetailTask().execute(videoId);
    }

//    private class VideoDetailTask extends AsyncTask<String, Void, YTVideoStatistics> {
//
//
//        @Override
//        protected void onPreExecute() {
//            super.onPreExecute();
//            llVideoDetail.setVisibility(View.INVISIBLE);
//            pbVideoProgress.setVisibility(View.VISIBLE);
//        }
//
//        @Override
//        protected YTVideoStatistics doInBackground(String... videoId) {
//
////            String part = "statistics";
//            String part = "snippet,contentDetails,statistics";
//            String key = YTConfig.getApiKey();
//            String response = ApiCall.GET(YTNetwork.getVideoStatistics
//                    , ParamBuilder.getStatistics(part, videoId[0], key));
//
//            return JsonParser.parseStatistics(response);
//        }
//
//        @Override
//        protected void onPostExecute(YTVideoStatistics response) {
//            super.onPostExecute(response);
//            llVideoDetail.setVisibility(View.VISIBLE);
//            pbVideoProgress.setVisibility(View.GONE);
//            if (TextUtils.isEmpty(response.getError()) && response.getList() != null
//                    && response.getList().size() > 0) {
//                YTVideoStatistics ytVideoModel = response.getList().get(0);
//                updateVideoUi(ytVideoModel);
//
//            } else {
//                llVideoDetail.setVisibility(View.GONE);
//                Toast.makeText(YTBaseActivity.this, response.getError(), Toast.LENGTH_SHORT).show();
//            }
//        }
//    }

    private void updateVideoUi(YTVideoStatistics ytVideoModel) {
        videoPublishedAt = ytVideoModel.getPublishedAt();
        videoTitle = ytVideoModel.getTitle();
        videoDescription = ytVideoModel.getDescription();
        updateVideoDetailInUI();

        tvVideoViews.setText(ytVideoModel.getViewCount());
        tvVideoLikes.setText(ytVideoModel.getLikeCount());
        tvVideoDisLikes.setText(ytVideoModel.getDislikeCount());
        llVideoDetail.setVisibility(View.VISIBLE);
    }

    @Override
    public void onConfigurationChanged(@NonNull Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
//        if (youTubePlayer != null && youTubePlayer.isPlaying()) {
//            if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {
//                youTubePlayer.setFullscreen(false);
//            } else if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
//                youTubePlayer.setFullscreen(true);
//            }
//        }
    }

}
