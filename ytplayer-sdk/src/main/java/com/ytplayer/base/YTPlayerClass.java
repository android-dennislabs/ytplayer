package com.ytplayer.base;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;

import com.config.config.ConfigManager;
import com.ytplayer.YTPlayer;
import com.ytplayer.YTUtility;
import com.ytplayer.adapter.YTVideoModel;
import com.ytplayer.util.YTConfig;
import com.ytplayer.util.YTDbHelper;
import com.ytplayer.util.YTListener;

import java.util.ArrayList;

/**
 * @author Created by Abhijit on 2/6/2018.
 */
public class YTPlayerClass implements YTPlayer {

    private Typeface typeface;
    private ConfigManager configManager;

    private static YTPlayerClass instance;
    private final Context context;
    private VideoType playerType;
    private YTDbHelper dbHelper;//

    private boolean isMarkFavouriteDisable = false;
    private boolean isMarkBookmarkDisable = false;

    private YTPlayerClass(Context context, String googleApiKey) {
        this.context = context;
        this.playerType = VideoType.OPEN_INTERNAL_PLAYER;
        YTConfig.setApiKey(googleApiKey);
    }

    public static YTPlayerClass Builder(Context context, String googleApiKey) {
        if (instance == null) {
            instance = new YTPlayerClass(context, googleApiKey);
        }
        return instance;
    }

    public static YTPlayerClass Builder() throws Exception {
        if (instance == null) {
            throw new Exception("Use getInstance() method to get the single instance of this class.");
        }
        return instance;
    }

    @Override
    public YTPlayerClass setConfigManager(ConfigManager configManager) {
        this.configManager = configManager;
        return this;
    }

    @Override
    public ConfigManager getConfigManager() {
        return configManager;
    }

    @Override
    public void openVideo(String videoId) {
        openVideo(videoId, false);
    }

    /**
     * Used for In app open
     */
    @Override
    public void openVideo(String videoId, boolean isDetailVisible) {
        YTConfig.setVideoId(videoId);
        if (playerType == VideoType.OPEN_INTERNAL_PLAYER) {
            YTUtility.openInternalYoutubePlayer(context);
        }
        if (playerType == VideoType.OPEN_INTERNAL_SLIDING_PLAYER) {
            YTUtility.openInternalYoutubePlayer(context, isDetailVisible);
        }
    }

    /**
     * Used for External app open
     */
    @Override
    public void openVideo(Activity activity, String videoId, boolean isDetailVisible) {
        YTUtility.openExternalYoutubeVideoPlayer(activity, YTConfig.getApiKey(), YTConfig.getVideoId());
    }

    @Override
    public Typeface getTypeface() {
        return typeface;
    }

    @Override
    public YTPlayerClass setTypeface(Typeface typeface) {
        this.typeface = typeface;
        return this;
    }

    @Override
    public YTPlayerClass maxListItemsCount(int maxResultsCount) {
        YTConfig.setMaxResultsCount(maxResultsCount);
        return this;
    }

    @Override
    public YTPlayerClass setPlayerType(VideoType playerType) {
        this.playerType = playerType;
        return this;
    }

    @Override
    public void openPlaylistExternal(Activity activity, String playlistId) {
        YTConfig.setPlaylistId(playlistId);
        YTUtility.openExternalYoutubePlaylistPlayer(activity, YTConfig.getApiKey(), YTConfig.getPlaylistId());
    }

    @Override
    public void openViewPlaylist(ArrayList<YTVideoModel> playlist) {
        openViewPlaylist(null, playlist);
    }

    @Override
    public void openViewPlaylist(String playerName, ArrayList<YTVideoModel> playlist) {
        YTUtility.openInternalYoutubePlaylistPlayer(context, playerName, playlist);
    }

    @Override
    public void openPlaylist(String host, String playerName, String playListId) {
        YTUtility.openInternalYoutubeByPlaylistId(context, host, playerName, playListId);
    }

    /**
     * @param playerName Toolbar title name
     * @param json       contains playlist ids , videos ids and mix ranking for both
     */
    @Override
    public void openYtByPlayListVideos(String playerName, String host, String json) {
        YTUtility.openInternalYoutubeByPlayListVideos(context, host, playerName, json);
    }

    @Override
    public void openPlaylistMultipleIds(String playerName, String host, String playListIds) {
        openPlaylistMultipleIds(0, playerName, host, playListIds);
    }

    /**
     * @param catId       subject category id
     * @param playerName  Toolbar title name
     * @param playListIds Playlist ids in comma separated
     */
    @Override
    public void openPlaylistMultipleIds(int catId, String playerName, String host, String playListIds) {
        YTUtility.openInternalYoutubeByPlayListMultipleIds(context, catId, host, playerName, playListIds);
    }

    @Override
    public void openBookmarkChannels(String title) {
        YTUtility.openBookmarkChannels(context, title);
    }

    @Override
    public void openBookmarkVideos(String title) {
        YTUtility.openBookmarkVideos(context, title);
    }

    @Override
    public void openSinglePlayVideoActivity(YTVideoModel videoModel) {
        YTUtility.openYoutubePlaySingleVideoActivity(context, videoModel);
    }

    @Override
    public void openChannel(String playerName, String channelId) {
        YTUtility.openInternalYoutubeByChannelId(context, playerName, channelId);
    }

//    public void openSearch(String youtubeChannelId) {
//        YTUtility.openInternalYoutubePlaylistPlayer(context, youtubeChannelId);
//    }

    @Override
    public void openSubscribeOption(String youtubeChannelId) {
        YTUtility.openInternalYoutubeSubscribe(context, youtubeChannelId);
    }

    private YTListener.OnHistoryListener mHistoryListener;

    @Override
    public YTPlayerClass addHistoryListener(YTListener.OnHistoryListener callback) {
        mHistoryListener = null;
        mHistoryListener = callback;
        return this;
    }

    @Override
    public YTListener.OnHistoryListener getHistoryListener() {
        return mHistoryListener;
    }

    @Override
    public void removeHistoryListener() {
        mHistoryListener = null;
    }

    @Override
    public void dispatchHistoryUpdated(YTVideoModel model) {
        if (mHistoryListener != null) {
            mHistoryListener.onMaintainHistory(model);
        }
    }

    @Override
    public void insertHistory(YTVideoModel model) {
        dispatchHistoryUpdated(model);
    }

    @Override
    public Context getContext() {
        return context;
    }

    @Override
    public YTDbHelper getDBObject() {
        if (dbHelper == null) {
            dbHelper = YTDbHelper.getInstance(context);
        }
        return dbHelper;
    }

    @Override
    public boolean isMarkFavouriteDisable() {
        return isMarkFavouriteDisable;
    }

    @Override
    public YTPlayerClass setMarkFavouriteDisable(boolean markFavouriteDisable) {
        isMarkFavouriteDisable = markFavouriteDisable;
        return this;
    }

    @Override
    public boolean isMarkBookmarkDisable() {
        return isMarkBookmarkDisable;
    }

    @Override
    public YTPlayerClass setMarkBookmarkDisable(boolean markBookmarkDisable) {
        isMarkBookmarkDisable = markBookmarkDisable;
        return this;
    }
}
