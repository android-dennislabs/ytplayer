package com.ytplayer.sample;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

public class BaseActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        registerReceiverCallBacks();
        callConfig();

    }

    private void registerReceiverCallBacks() {
        AppApplication.getInstance().getConfigManager().getNetworkMonitor().register(this);
        AppApplication.getInstance().registerReceiver();
    }

    @Override
    public void finish() {
        AppApplication.getInstance().unregisterReceiver();
        AppApplication.getInstance().getConfigManager().getNetworkMonitor().unregister(this);
        super.finish();
    }

    private void callConfig() {
        if (AppApplication.getInstance() != null) {
            if ((AppApplication.getInstance().getConfigManager() == null
                    || !AppApplication.getInstance().getConfigManager().isConfigLoaded())) {
//            showConnectingTextView();
                AppApplication.getInstance().initOperations();
            } else if (AppApplication.getInstance().getConfigManager() != null
                    && AppApplication.getInstance().getConfigManager().isConfigLoaded()) {
//            showSuccessConnectionTextView();
            }
        }
    }
}
