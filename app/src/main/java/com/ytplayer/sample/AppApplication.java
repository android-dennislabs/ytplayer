package com.ytplayer.sample;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.text.TextUtils;

import com.adssdk.AdsSDK;
import com.config.config.ConfigConstant;
import com.config.config.ConfigManager;
import com.config.util.ConfigUtil;
import com.config.util.Logger;
import com.helper.application.ActivityTrackingApplication;
import com.ytplayer.YTPlayer;
import com.ytplayer.adapter.YTVideoModel;


public class AppApplication extends ActivityTrackingApplication {

    private static AppApplication _instance;
    private LoginSdk loginSdk;
    private ConfigManager configManager;
    private AdsSDK adsSDK;

    public static AppApplication getInstance() {
        return _instance;
    }

    @Override
    public boolean isDebugMode() {
        return BuildConfig.DEBUG;
    }

    @Override
    public void onCreate() {
//        turnOnStrictMode();
        super.onCreate();
        _instance = this;
        configManager = ConfigManager.getInstance(this, ConfigUtil.getSecurityCode(this), BuildConfig.DEBUG);
        adsSDK = new AdsSDK(this, true, getPackageName());

    }

    @Override
    public void initLibs() {

    }

//    private void turnOnStrictMode() {
//        if (BuildConfig.DEBUG) {
//            StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder()
//                    .detectDiskReads()
//                    .detectDiskWrites()
//                    .detectNetwork()   // or .detectAll() for all detectable problems
//                    .penaltyLog()
//                    .build());
//            StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder()
//                    .detectLeakedSqlLiteObjects()
//                    .detectLeakedClosableObjects()
//                    .penaltyLog()
//                    .build());
//        }
//    }


    public LoginSdk getLoginSdk() {
        if (loginSdk == null) {
//            initLoginSDK();
        }
        return loginSdk;
    }


    /**
     * register from MainActivity
     */
    public void registerReceiver() {
        registerReceiver(configReceiver, new IntentFilter(getPackageName() + ConfigConstant.CONFIG_LOADED));
    }

    /**
     * unregister from MainActivity
     */
    public void unregisterReceiver() {
        Logger.e("appApplication", "onTerminate");
        try {
            if (configReceiver != null)
                unregisterReceiver(configReceiver);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private boolean isSync = false;

    BroadcastReceiver configReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (!isSync) {
                syncData();
                isSync = true;
            }
        }
    };

    public void syncData() {
        if (configManager != null
                && configManager.getHostAlias() != null
                && configManager.getHostAlias().size() > 0
                && configManager.getHostAlias().get(ConfigConstant.HOST_ANALYTICS) != null) {
        }
    }

    public void initOperations() {
        isSync = false;
        configManager.loadConfig();
    }

    public ConfigManager getConfigManager() {
        return configManager;
    }



    private YTPlayer ytPlayer;

    public YTPlayer getYTPlayer(Context context) {
        if (ytPlayer == null) {
            String youtubeKey = getString(R.string.google_api_key);
            if (!TextUtils.isEmpty(youtubeKey)) {
                ytPlayer = YTPlayer.getInstance(context, youtubeKey)
                        .setConfigManager(configManager)
                        .setPlayerType(YTPlayer.VideoType.OPEN_INTERNAL_PLAYER);
                //.maxListItemsCount(maxResultCount)
            }
        }
        return ytPlayer;
    }

    public void openPlaylist(Context context, int catId, String host, String title, String playlistIds) {
        try {
            getYTPlayer(context).openPlaylistMultipleIds(catId, title, host, playlistIds);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void openPlaylistAndVideo(Context context, String host, String title, String json) {
        try {
            getYTPlayer(context).openYtByPlayListVideos(title, host, json);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void openSinglePlayVideoActivity(Context context, YTVideoModel item) {
        try {
            getYTPlayer(context).openSinglePlayVideoActivity(item);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
