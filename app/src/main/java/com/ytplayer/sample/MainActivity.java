package com.ytplayer.sample;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.ytplayer.activity.playlist.FullscreenExampleActivity;
import com.ytplayer.adapter.YTVideoModel;
import com.ytplayer.util.YTType;

public class MainActivity extends BaseActivity {

    private String playListIds = "PLIQkEkEOvayLNLWXsCcQRpBWTUBiByh6_,PLIQkEkEOvayKVu39wdXmwH1nM0MXw0rZ-,PLZ4yeuWi-T_N4axIPSwQItX2G8gWfDOeV,PLLBhGjrtIYzj4cg1wWzytI3Bbl9tEZrl6,PLeBYmJvsxglPU-ktCcDPuohjbBrMoogMd,PLF7C-DWw7CnNv1Rg8PQ_kEKZzY5ztpw7u,PLF7C-DWw7CnOv_fDRms1f_QTHqU4c7gVg,PLF7C-DWw7CnPasarqoA2ab1VxS_B2hhyZ,PLF7C-DWw7CnNlBqj5BgnBwuCjDUpFy0Wz,PLF7C-DWw7CnO1grR13WkFCvIbiUbGrd8e,PLF7C-DWw7CnMFW1bJ7uNK0DMsNvDHvFeP,PLF7C-DWw7CnN2Vf2FU77NOk7uoXWW4ycI,PLF7C-DWw7CnMNNKrnlkUpeNpuuBVWr1GM,PLF7C-DWw7CnOrGbHLBD-L75-g_vzj7zB8,PLF7C-DWw7CnMe5CgL6l25XFzFYzrBqxB0,PLF7C-DWw7CnPRfjULp-aQns98NmHYBN9p,PLF7C-DWw7CnPwCSx8KxsJJvgLp9415W_o,PL8dPuuaLjXtN0ge7yDk_UA0ldZJdhwkoV,PLAD5B880806EBE0A4,PLCzaIJYXP5YfqdCHEE9D6NjOb6ibHzL6K,PL7AAT-ai0VD6XPIWhPG_nRpSimJBv6ePX,PLyQSN7X0ro2314mKyUiOILaOC2hk6Pc3j";

    private String json = "{\"video_playlist_ids\":\"\",\"video_ids\":\"G2deQFuo7C4,yRT7CFSTz_U,59ag9Q7-sow\",\"item_list\":[{\"id\":\"G2deQFuo7C4\",\"type\":\"video\",\"rank\":1},{\"id\":\"yRT7CFSTz_U\",\"type\":\"video\",\"rank\":2},{\"id\":\"59ag9Q7-sow\",\"type\":\"video\",\"rank\":3}]}";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        (findViewById(R.id.btn_open_play)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppApplication.getInstance().openPlaylistAndVideo(MainActivity.this, "translater_host", "Physics", json);
            }
        });
        (findViewById(R.id.btn_open_play2)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getYoutubeItem();
                AppApplication.getInstance().openSinglePlayVideoActivity(MainActivity.this, getYoutubeItem());
//                startActivity(new Intent( MainActivity.this , FullscreenExampleActivity.class ));
            }
        });
    }

    public void onOpenPlayList(View view) {
        String host = "translater_host";
        String title = "Mathematics";
        String multiPlayListIds = "PLCtUyOrCJbxxkL5dPyLJi9UmUlrP-etG3,PL46TqiNVZpK8JcgaIq2wgUZWtNvZvnFlc,PL8yzSJkwFafN3Z47GwKTGx1OnMk8mq38L,PLOiyIhFZ-Rh8EnAR4G8Mxt-bL43dh3Qfr,PL34G4huOdp_VA8WlyVyMggmCzJpTg-_oQ,PLOiyIhFZ-Rh_MQR7FXC2HdBTdiYYGTOaQ,PL_A4M5IAkMad5zB0Dh6gUw1eYK8dN7hP7,PL_A4M5IAkMads8vQF8GLy8JdbuUyafYgY,PL_A4M5IAkMafhPkzpa16mbUDqD8d7bBhL,PL_A4M5IAkMafsM4VwfCbZ8Oa4i_96R09n,PL_A4M5IAkMaerL1K-p5sRRdzjNrqn72Rg,PL_A4M5IAkMacqcUtkJPTPXSrvNm_5NK-v,PL_A4M5IAkMaf3M7rSq9M4NmLACdYuCQ_7,PL_A4M5IAkMadiR6WFaUTQegpdKUJRD90D,PL_A4M5IAkMaeT3qAAgcvUXiKnM044FO44,PL_A4M5IAkMae6_gCoLFltXl3bmtZmOvjQ,PL_A4M5IAkMaeaLAU22ViTSvk3T7AWxnT_,PL_A4M5IAkMaexM2nxZt512ESPt83EshJq,PL_A4M5IAkMads1bsxLYBoJOLA3bWsY7mK,PL_A4M5IAkMafj2vfy6rQHtTzgA_TnJsZ_,PL_A4M5IAkMaeewzwCJPpL65y1HV0VAiC4,PLTCyZxgpy_PXsjKTtiLEYu_xlehhtProB,PL_A4M5IAkMafig81EHnu345hUAHXOhe46,PL_A4M5IAkMadF2rExT0C-TvtH2vn3dLGR,PL_A4M5IAkMaf5Ga3nQJe-gg-0zXG77YRB,PL_A4M5IAkMaeu--QWWngkEI10RKOXf8TF,PL_A4M5IAkMacSgRRlEkUB9v-gE1yxw_rG,PLw3N9BlswxRdDO_GIN9B-XVBGArBu6qpT,PLs4jXBolrbgMUxbEpJeb8PoOoH9W1e74Z,PLZqhaOmR6gUELXkGBvyHafUj6UYT1fWAf,PLDJktDT5uc9vqFCqa19xUkhZ_IALfVDz_,PL34G4huOdp_WXoB3J1bsz2rvMgmZkkO-g,PLtfEipLJrhIGakw7cSJt6Lm_4qpcvn9H1,PL34G4huOdp_XPYHcLgbk85zu2j4EK94Zs,PL34G4huOdp_VA8WlyVyMggmCzJpTg-_oQ,PLLoUziOB0bCQw-oGgDEZUSWB6atW9CFgj,PLbu_fGT0MPssea8Yb0bov4v0o_0xfT9Qr,PLx2L6yjUEa7uV74vWZ8BZbk2ZRe6Axx1H";
        AppApplication.getInstance().openPlaylist(MainActivity.this, 123, host, title, multiPlayListIds);
    }

    private YTVideoModel getYoutubeItem() {
        YTVideoModel item = new YTVideoModel();
        item.setChannelId("UCMwSslwMHuirxRLP4L-7xUQ");
        item.setChannelTitle("Varnika Gupta");
        item.setDescription("Physics, Class XII  Chapter : Electric charge and field    Topic : electric dipole    Classroom lecture by Pradeep Kshetrapal. Language : English mixed with Hindi.");
        item.setImage("https://i.ytimg.com/vi/NN8Qx-0qMa0/mqdefault.jpg");
        item.setPublishedAt("2016-06-06T10:24:24Z");
        item.setTitle("XII-1-8 Electric dipole (2016)Pradeep Kshetrapal Physics");
        item.setVideoCount(7);
        item.setVideoId("NN8Qx-0qMa0");
        item.setYtType(YTType.VIDEO);
        item.setHost("translater_host");
        return item;
    }

    public void onOpenJeePhysics(View view) {
        String host = "translater_host";
        String title = "Physics";
        String multiPlayListIds = "PLk5kuyX3xzdY3sH11naVgw_RBuONQ03bq,PLCtUyOrCJbxycyaD58Jd9aMqQ1ERtL0pQ,PLKb9HOyS0VGRm626tDguoXb3i9mdQaAF1,PL34G4huOdp_WjwlzmlprKrE6R-NsSbhw3,PL34G4huOdp_Xhj4M0_TLagLFXcVmzlmYM,PL8yzSJkwFafPkONgqhZ3Vt17xXI2kbMt6,PL8yzSJkwFafN9kB0fIZ8D1SMZG5NZjev_,PLPuQfCUxtUA9t8XGEx-MRQI_qsPIzuSyg,PLYPcEGSx6fEwyYfxf7F7mZNnK_hhuf5P2,PL34G4huOdp_UVZV5samwKTHo0Au5HeusU,PLOiyIhFZ-Rh-1XrOm20b_4ZRSNJ_uRcDp,PLOiyIhFZ-Rh-zIQsi_b7QEHVl_6rlkRT5,PLOiyIhFZ-Rh9ZRi1zWt7KTWsKOwQZYExo,PL8z1Dmsl0yvsPhGKfhSIKyNd9JX4yzRPc,PL8yzSJkwFafNYKl9L8cn1-J9IdbBJa2wA,PLAD5B880806EBE0A4,PLvM3c3biDoSaq40EclmRgZnhTBrFXFE3k,PLYPcEGSx6fEzFimanZhyYFD2cOVvgutwx,PLJZk2__oyAljqv1f2ROVMJ56Ucdbhc_SE,PL8yzSJkwFafOxX3aypSgNAYLDnCO9oEiK,PLL7jukBZRfqyX1g9Ao3YG_9j_wowp8NVN,PL_0lj9rby3g0CWdLYT8ojVMf6EMfWG90Q,PL34G4huOdp_Vc37DhtWQTjMrCkcifUfJc,PLLoUziOB0bCSjRh8F8CJBBb51s2BnNwbt,PLYVDsiuOZP5rqs4W2gN_lnAXL3AcGc5O0,PLYVDsiuOZP5om-Dr8S1-10KrMa4vH738M,PLYVDsiuOZP5pPq2iq_laoefweIqDYpDyo,PLYVDsiuOZP5ptBNrpIa6yXIaBId0yHsuL,PLYVDsiuOZP5pFmAoduzQy3sxwcki-D-BH,PLYVDsiuOZP5px9BfU3X8PPOXqfUxIr1al,PLYVDsiuOZP5onjQVfPF07-xEU7wiSywJc,PLYVDsiuOZP5pPR-c6OMwPaFB4jmcDOIya,PLYVDsiuOZP5qmrouCi58e0fbr2_x7__TY,PLYVDsiuOZP5rIsH_vq82f6oOskpSFfMmL,PLYVDsiuOZP5q0u0yR03SNdhswS2PmwT02,PLYVDsiuOZP5oK53QqLAVxj5gCPpyuK8-b,PLYVDsiuOZP5ojPF7PTwzn-6TDnFs3aSNC,PLYVDsiuOZP5q94KNzbdwaM1Pg-oR9bKYJ,PLYVDsiuOZP5otIssIBcggs5gd-t6nWQsF,PLYVDsiuOZP5rYYDPBw5GEM5iq3aA9FoYm,PLYVDsiuOZP5pBiXzoAFP2KVIZorRGdYZ2,PLYVDsiuOZP5r2VyNRnxIcihxIErobLnJH,PLYVDsiuOZP5qClrxbX_gQIRWbbVUc81-a,PLYVDsiuOZP5p1wqNBMFQPeYA1rV26WVU2,PLYVDsiuOZP5oNMA20ENGpMZn205nKjp6t,PLYVDsiuOZP5rzIZ99BsayQWooqwjP5PZ2,PLYVDsiuOZP5rO0KbnU2gyHfmlRr996DFo,PLYVDsiuOZP5pWc0SxrnaUVHaMT06mdo4a,PLYVDsiuOZP5pIrz7YXgTavrdbioCZDBAT,PLYVDsiuOZP5reV1N7Ezy_E8Fv3HXkcCCQ,PLYVDsiuOZP5rNtTnGCeR-qMiWsnY3Z9c0,PLYVDsiuOZP5oDZNTKTHdDXk2yeDd5USgr,PLYVDsiuOZP5ruG0Iy2QZJLMB48-j0fDT3,PLYVDsiuOZP5rOxjiDU81nW07oYApW60IQ,PLYVDsiuOZP5pStNOS-TnVtD8dro1IDpI4,PLYVDsiuOZP5oCFyv-NjoVzilZz-aHgwwA,PLYVDsiuOZP5pduTwG-xUqCf73yEwR-G9Q,PLYVDsiuOZP5qn4WTpMvNxh71A5m2euYFH,PLYVDsiuOZP5oSA1adbrafXl5G-_ZEyUg8,PLYVDsiuOZP5pnPMEJ_pCBTHmYZkFgzZpZ,PLYVDsiuOZP5p1efgLGHU_fT3BnOn2PeT9,PLYVDsiuOZP5oARIu2zma1w7BbjekSXULp,PLYVDsiuOZP5o58QuOJsZ6XNazPba5y9Vk,PLYVDsiuOZP5pDolBtG83KK5GNiFP2Wdwo,PLYVDsiuOZP5ro6oNQNgb4lEiYUQf6O5ix,PLYVDsiuOZP5puf3ODDtJeoKVcw_nn4sHe,PLYVDsiuOZP5oa3Xt30jyJf8TgjRdE_Ffo,PLYVDsiuOZP5ophSnSLgD7GoYz6Zoyieka,PLYVDsiuOZP5pKJ6B0q_hxAzXgzSjpaqum,PLYVDsiuOZP5oX_utM6U1WELiA4lAhmd07,PLYVDsiuOZP5riuB-SQjeHsZ3nvky5rzFq,PLYVDsiuOZP5otvXTVJUzf4leXA4drAAVP,PLYVDsiuOZP5pSLMHtzNoz8Cgkah083R3k,PLYVDsiuOZP5qXPy3IJcqyu3dO_0lMB_Vu,PLYVDsiuOZP5pNp6mKyhYm_dyKhQa89d-U,PLYVDsiuOZP5rYBwzytQnnLZ2PJg9TTJWV,PLYVDsiuOZP5pAhz46m8KxKQGyHKU8Xq24,PLYVDsiuOZP5p1dAoyw-cq5Sho6jKzRO68,PLYVDsiuOZP5ryU-IY_OGQmaYe7hj5WaXf,PLYVDsiuOZP5oFZy9pRC0Js6AR4CQwd0TA,PLYVDsiuOZP5q1zluvt1QzvSr8ZhVFSxXC,PL34G4huOdp_XFvT9AMNYK4cNQlKNTngYu,PL34G4huOdp_X-AOSsodMcGYEaNcqIIi4E,PL34G4huOdp_VO1WHd9r_qGco0FcyvSQZs,PL34G4huOdp_UlXYxuMNfHv43EBk6F7-tu,PL34G4huOdp_Wht2Xj4W2nLauo46SFgKXa,PL34G4huOdp_VoUct_Lv9WDCsVJdkgnWFk,PL34G4huOdp_WPNPkUPZ4HVdH_5NNb5bPS,PL34G4huOdp_WS9laY5Y3NtDgmOD6d3cEs,PL34G4huOdp_Uas5pMPv6R6pm-mrArOjw5,PL34G4huOdp_Wfz-zqbfSH0wQop9xKAQ47,PL34G4huOdp_UkERYJbyHjLqIi25s2DFUM,PL34G4huOdp_XYfa8o-GY1-498LKGfgNMI,PL34G4huOdp_V-qVEVeyP1u2G8RrCxNPwr,PL34G4huOdp_XCsoP4DrKxPlPLo2XVhpFj,PL34G4huOdp_UtvOP7diRJCZrmUQ4bHIH8,PL34G4huOdp_WadE6J0FIIuqi7Rp95e-oL,PL34G4huOdp_VItVmR5z5sxEmEmQuKnfOE,PL34G4huOdp_XWtHssQmAIGmdDZPEjJco6,PL34G4huOdp_VCnb0Y2z_Iy9K4PmlaeuGv,PL34G4huOdp_XbS6RiHRE8-vxCplAyUXyx,PL34G4huOdp_UGTccGTSnySM4stwp20egr,PL34G4huOdp_Wj-ptKEB3UmWHY_g2BVTaR,PL34G4huOdp_XgxyjMvwnCARyLr2DmBRLA,PL34G4huOdp_WPVnSl6XXRnN36bkkIU0TT,PL34G4huOdp_WAjqqC1_7drCFs58XMQExK,PL34G4huOdp_U2IuaAfaK05tlSdzdFHLch,PL34G4huOdp_V4yQJbFhlasFAE7d7I8DHl,PL34G4huOdp_VDbx1atz5ZZM5LvcLhoGr8,PL34G4huOdp_UGhwhk7v3WnEDXQQ2X1mIF,PL34G4huOdp_Xg9lleMZb-izZyUGY3QwwO,PL34G4huOdp_Vp0SIkgL1qEIAsqq52IrcO,PL34G4huOdp_Xwd-nQXLFDDYRAtF2FGlka,PL34G4huOdp_VWca6nsLReplkSr5Y4nVRx,PL34G4huOdp_UcYrgGNvhZ6fJRMJU6uurO,PL34G4huOdp_Wc_MrUmD_L6t3Vst3iAraS";
        AppApplication.getInstance().openPlaylist(MainActivity.this, 124, host, title, multiPlayListIds);
    }

    public void onBookmarkChannels(View view) {
        AppApplication.getInstance().getYTPlayer(this).openBookmarkChannels(getString(com.ytplayer.R.string.channel_bookmarks));
    }

    public void onBookmarkVideos(View view) {
        AppApplication.getInstance().getYTPlayer(this).openBookmarkVideos("Video Bookmarks");
    }
}
