# YTPlayer Sdk

#### Library size is : 0kb in version above 3.3

## Setup

Add this to your project build.gradle
``` gradle
allprojects {
    repositories {
        maven {
            url "https://jitpack.io"
        }
    }
}
```

#### Dependency
[![](https://jitpack.io/v/org.bitbucket.android-dennislabs/ytplayer.svg)](https://jitpack.io/#org.bitbucket.android-dennislabs/ytplayer)
```gradle
dependencies {
        implementation 'org.bitbucket.android-dennislabs:ytplayer:3.1'
}
```